! function(e) {
    var t = {};
    var actheme = jQuery(".act_themes").val();

    function o(a) {
        if (t[a]) return t[a].exports;
        var n = t[a] = {
            i: a,
            l: !1,
            exports: {}
        };
        return e[a].call(n.exports, n, n.exports, o), n.l = !0, n.exports
    }
    o.m = e, o.c = t, o.d = function(e, t, a) {
        o.o(e, t) || Object.defineProperty(e, t, {
            enumerable: !0,
            get: a
        })
    }, o.r = function(e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }, o.t = function(e, t) {
        if (1 & t && (e = o(e)), 8 & t) return e;
        if (4 & t && "object" == typeof e && e && e.__esModule) return e;
        var a = Object.create(null);
        if (o.r(a), Object.defineProperty(a, "default", {
                enumerable: !0,
                value: e
            }), 2 & t && "string" != typeof e)
            for (var n in e) o.d(a, n, function(t) {
                return e[t]
            }.bind(null, n));
        return a
    }, o.n = function(e) {
        var t = e && e.__esModule ? function() {
            return e.default
        } : function() {
            return e
        };
        return o.d(t, "a", t), t
    }, o.o = function(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }, o.p = "", o(o.s = 7)
}([function(e, t, o) {
    "use strict";
    const a = o(3),
        n = ["__proto__", "prototype", "constructor"];

    function s(e) {
        const t = e.split("."),
            o = [];
        for (let e = 0; e < t.length; e++) {
            let a = t[e];
            for (;
                "\\" === a[a.length - 1] && void 0 !== t[e + 1];) a = a.slice(0, -1) + ".", a += t[++e];
            o.push(a)
        }
        return o.some(e => n.includes(e)) ? [] : o
    }
    e.exports = {
        get(e, t, o) {
            if (!a(e) || "string" != typeof t) return void 0 === o ? e : o;
            const n = s(t);
            if (0 !== n.length) {
                for (let t = 0; t < n.length; t++) {
                    if (!Object.prototype.propertyIsEnumerable.call(e, n[t])) return o;
                    if (null == (e = e[n[t]])) {
                        if (t !== n.length - 1) return o;
                        break
                    }
                }
                return e
            }
        },
        set(e, t, o) {
            if (!a(e) || "string" != typeof t) return e;
            const n = e,
                r = s(t);
            for (let t = 0; t < r.length; t++) {
                const n = r[t];
                a(e[n]) || (e[n] = {}), t === r.length - 1 && (e[n] = o), e = e[n]
            }
            return n
        },
        delete(e, t) {
            if (!a(e) || "string" != typeof t) return !1;
            const o = s(t);
            for (let t = 0; t < o.length; t++) {
                const n = o[t];
                if (t === o.length - 1) return delete e[n], !0;
                if (e = e[n], !a(e)) return !1
            }
        },
        has(e, t) {
            if (!a(e) || "string" != typeof t) return !1;
            const o = s(t);
            if (0 === o.length) return !1;
            for (let t = 0; t < o.length; t++) {
                if (!a(e)) return !1;
                if (!(o[t] in e)) return !1;
                e = e[o[t]]
            }
            return !0
        }
    }
}, function(e, t, o) {
    var a;
    a = function() {
        var e = JSON.parse('{"$":"dollar","%":"percent","&":"and","<":"less",">":"greater","|":"or","¢":"cent","£":"pound","¤":"currency","¥":"yen","©":"(c)","ª":"a","®":"(r)","º":"o","À":"A","Á":"A","Â":"A","Ã":"A","Ä":"A","Å":"A","Æ":"AE","Ç":"C","È":"E","É":"E","Ê":"E","Ë":"E","Ì":"I","Í":"I","Î":"I","Ï":"I","Ð":"D","Ñ":"N","Ò":"O","Ó":"O","Ô":"O","Õ":"O","Ö":"O","Ø":"O","Ù":"U","Ú":"U","Û":"U","Ü":"U","Ý":"Y","Þ":"TH","ß":"ss","à":"a","á":"a","â":"a","ã":"a","ä":"a","å":"a","æ":"ae","ç":"c","è":"e","é":"e","ê":"e","ë":"e","ì":"i","í":"i","î":"i","ï":"i","ð":"d","ñ":"n","ò":"o","ó":"o","ô":"o","õ":"o","ö":"o","ø":"o","ù":"u","ú":"u","û":"u","ü":"u","ý":"y","þ":"th","ÿ":"y","Ā":"A","ā":"a","Ă":"A","ă":"a","Ą":"A","ą":"a","Ć":"C","ć":"c","Č":"C","č":"c","Ď":"D","ď":"d","Đ":"DJ","đ":"dj","Ē":"E","ē":"e","Ė":"E","ė":"e","Ę":"e","ę":"e","Ě":"E","ě":"e","Ğ":"G","ğ":"g","Ģ":"G","ģ":"g","Ĩ":"I","ĩ":"i","Ī":"i","ī":"i","Į":"I","į":"i","İ":"I","ı":"i","Ķ":"k","ķ":"k","Ļ":"L","ļ":"l","Ľ":"L","ľ":"l","Ł":"L","ł":"l","Ń":"N","ń":"n","Ņ":"N","ņ":"n","Ň":"N","ň":"n","Ō":"O","ō":"o","Ő":"O","ő":"o","Œ":"OE","œ":"oe","Ŕ":"R","ŕ":"r","Ř":"R","ř":"r","Ś":"S","ś":"s","Ş":"S","ş":"s","Š":"S","š":"s","Ţ":"T","ţ":"t","Ť":"T","ť":"t","Ũ":"U","ũ":"u","Ū":"u","ū":"u","Ů":"U","ů":"u","Ű":"U","ű":"u","Ų":"U","ų":"u","Ŵ":"W","ŵ":"w","Ŷ":"Y","ŷ":"y","Ÿ":"Y","Ź":"Z","ź":"z","Ż":"Z","ż":"z","Ž":"Z","ž":"z","Ə":"E","ƒ":"f","Ơ":"O","ơ":"o","Ư":"U","ư":"u","ǈ":"LJ","ǉ":"lj","ǋ":"NJ","ǌ":"nj","Ș":"S","ș":"s","Ț":"T","ț":"t","ə":"e","˚":"o","Ά":"A","Έ":"E","Ή":"H","Ί":"I","Ό":"O","Ύ":"Y","Ώ":"W","ΐ":"i","Α":"A","Β":"B","Γ":"G","Δ":"D","Ε":"E","Ζ":"Z","Η":"H","Θ":"8","Ι":"I","Κ":"K","Λ":"L","Μ":"M","Ν":"N","Ξ":"3","Ο":"O","Π":"P","Ρ":"R","Σ":"S","Τ":"T","Υ":"Y","Φ":"F","Χ":"X","Ψ":"PS","Ω":"W","Ϊ":"I","Ϋ":"Y","ά":"a","έ":"e","ή":"h","ί":"i","ΰ":"y","α":"a","β":"b","γ":"g","δ":"d","ε":"e","ζ":"z","η":"h","θ":"8","ι":"i","κ":"k","λ":"l","μ":"m","ν":"n","ξ":"3","ο":"o","π":"p","ρ":"r","ς":"s","σ":"s","τ":"t","υ":"y","φ":"f","χ":"x","ψ":"ps","ω":"w","ϊ":"i","ϋ":"y","ό":"o","ύ":"y","ώ":"w","Ё":"Yo","Ђ":"DJ","Є":"Ye","І":"I","Ї":"Yi","Ј":"J","Љ":"LJ","Њ":"NJ","Ћ":"C","Џ":"DZ","А":"A","Б":"B","В":"V","Г":"G","Д":"D","Е":"E","Ж":"Zh","З":"Z","И":"I","Й":"J","К":"K","Л":"L","М":"M","Н":"N","О":"O","П":"P","Р":"R","С":"S","Т":"T","У":"U","Ф":"F","Х":"H","Ц":"C","Ч":"Ch","Ш":"Sh","Щ":"Sh","Ъ":"U","Ы":"Y","Ь":"","Э":"E","Ю":"Yu","Я":"Ya","а":"a","б":"b","в":"v","г":"g","д":"d","е":"e","ж":"zh","з":"z","и":"i","й":"j","к":"k","л":"l","м":"m","н":"n","о":"o","п":"p","р":"r","с":"s","т":"t","у":"u","ф":"f","х":"h","ц":"c","ч":"ch","ш":"sh","щ":"sh","ъ":"u","ы":"y","ь":"","э":"e","ю":"yu","я":"ya","ё":"yo","ђ":"dj","є":"ye","і":"i","ї":"yi","ј":"j","љ":"lj","њ":"nj","ћ":"c","ѝ":"u","џ":"dz","Ґ":"G","ґ":"g","Ғ":"GH","ғ":"gh","Қ":"KH","қ":"kh","Ң":"NG","ң":"ng","Ү":"UE","ү":"ue","Ұ":"U","ұ":"u","Һ":"H","һ":"h","Ә":"AE","ә":"ae","Ө":"OE","ө":"oe","฿":"baht","ა":"a","ბ":"b","გ":"g","დ":"d","ე":"e","ვ":"v","ზ":"z","თ":"t","ი":"i","კ":"k","ლ":"l","მ":"m","ნ":"n","ო":"o","პ":"p","ჟ":"zh","რ":"r","ს":"s","ტ":"t","უ":"u","ფ":"f","ქ":"k","ღ":"gh","ყ":"q","შ":"sh","ჩ":"ch","ც":"ts","ძ":"dz","წ":"ts","ჭ":"ch","ხ":"kh","ჯ":"j","ჰ":"h","Ẁ":"W","ẁ":"w","Ẃ":"W","ẃ":"w","Ẅ":"W","ẅ":"w","ẞ":"SS","Ạ":"A","ạ":"a","Ả":"A","ả":"a","Ấ":"A","ấ":"a","Ầ":"A","ầ":"a","Ẩ":"A","ẩ":"a","Ẫ":"A","ẫ":"a","Ậ":"A","ậ":"a","Ắ":"A","ắ":"a","Ằ":"A","ằ":"a","Ẳ":"A","ẳ":"a","Ẵ":"A","ẵ":"a","Ặ":"A","ặ":"a","Ẹ":"E","ẹ":"e","Ẻ":"E","ẻ":"e","Ẽ":"E","ẽ":"e","Ế":"E","ế":"e","Ề":"E","ề":"e","Ể":"E","ể":"e","Ễ":"E","ễ":"e","Ệ":"E","ệ":"e","Ỉ":"I","ỉ":"i","Ị":"I","ị":"i","Ọ":"O","ọ":"o","Ỏ":"O","ỏ":"o","Ố":"O","ố":"o","Ồ":"O","ồ":"o","Ổ":"O","ổ":"o","Ỗ":"O","ỗ":"o","Ộ":"O","ộ":"o","Ớ":"O","ớ":"o","Ờ":"O","ờ":"o","Ở":"O","ở":"o","Ỡ":"O","ỡ":"o","Ợ":"O","ợ":"o","Ụ":"U","ụ":"u","Ủ":"U","ủ":"u","Ứ":"U","ứ":"u","Ừ":"U","ừ":"u","Ử":"U","ử":"u","Ữ":"U","ữ":"u","Ự":"U","ự":"u","Ỳ":"Y","ỳ":"y","Ỵ":"Y","ỵ":"y","Ỷ":"Y","ỷ":"y","Ỹ":"Y","ỹ":"y","‘":"\'","’":"\'","“":"\\"","”":"\\"","†":"+","•":"*","…":"...","₠":"ecu","₢":"cruzeiro","₣":"french franc","₤":"lira","₥":"mill","₦":"naira","₧":"peseta","₨":"rupee","₩":"won","₪":"new shequel","₫":"dong","€":"euro","₭":"kip","₮":"tugrik","₯":"drachma","₰":"penny","₱":"peso","₲":"guarani","₳":"austral","₴":"hryvnia","₵":"cedi","₸":"kazakhstani tenge","₹":"indian rupee","₺":"turkish lira","₽":"russian ruble","₿":"bitcoin","℠":"sm","™":"tm","∂":"d","∆":"delta","∑":"sum","∞":"infinity","♥":"love","元":"yuan","円":"yen","﷼":"rial"}'),
            t = JSON.parse('{"de":{"Ä":"AE","ä":"ae","Ö":"OE","ö":"oe","Ü":"UE","ü":"ue"},"vi":{"Đ":"D","đ":"d"}}');

        function o(o, a) {
            if ("string" != typeof o) throw new Error("slugify: string argument expected");
            var n = t[(a = "string" == typeof a ? {
                    replacement: a
                } : a || {}).locale] || {},
                s = void 0 === a.replacement ? "-" : a.replacement,
                r = o.split("").reduce((function(t, o) {
                    return t + (n[o] || e[o] || o).replace(a.remove || /[^\w\s$*_+~.()'"!\-:@]+/g, "")
                }), "").trim().replace(new RegExp("[\\s" + s + "]+", "g"), s);
            return a.lower && (r = r.toLowerCase()), a.strict && (r = r.replace(new RegExp("[^a-zA-Z0-9" + s + "]", "g"), "").replace(new RegExp("[\\s" + s + "]+", "g"), s)), r
        }
        return o.extend = function(t) {
            for (var o in t) e[o] = t[o]
        }, o
    }, e.exports = a(), e.exports.default = a()
}, function(e, t, o) {
    "use strict";
    (function(e) {
        function a() {
            return (a = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var o = arguments[t];
                    for (var a in o) Object.prototype.hasOwnProperty.call(o, a) && (e[a] = o[a])
                }
                return e
            }).apply(this, arguments)
        }

        function n(e) {
            return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
                return typeof e
            } : function(e) {
                return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
            })(e)
        }
        var s, r = (s = function(e, t) {
            return (s = Object.setPrototypeOf || {
                    __proto__: []
                }
                instanceof Array && function(e, t) {
                    e.__proto__ = t
                } || function(e, t) {
                    for (var o in t) t.hasOwnProperty(o) && (e[o] = t[o])
                })(e, t)
        }, function(e, t) {
            function o() {
                this.constructor = e
            }
            s(e, t), e.prototype = null === t ? Object.create(t) : (o.prototype = t.prototype, new o)
        });
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.UnControlled = t.Controlled = void 0;
        var i, l = o(5),
            p = "undefined" == typeof navigator || !0 === e.PREVENT_CODEMIRROR_RENDER;
        p || (i = o(6));
        var u = function() {
                function e() {}
                return e.equals = function(e, t) {
                    var o = this,
                        a = Object.keys,
                        s = n(e),
                        r = n(t);
                    return e && t && "object" === s && s === r ? a(e).length === a(t).length && a(e).every((function(a) {
                        return o.equals(e[a], t[a])
                    })) : e === t
                }, e
            }(),
            c = function() {
                function e(e, t) {
                    this.editor = e, this.props = t
                }
                return e.prototype.delegateCursor = function(e, t, o) {
                    var a = this.editor.getDoc();
                    o && this.editor.focus(), t ? a.setCursor(e) : a.setCursor(e, null, {
                        scroll: !1
                    })
                }, e.prototype.delegateScroll = function(e) {
                    this.editor.scrollTo(e.x, e.y)
                }, e.prototype.delegateSelection = function(e, t) {
                    this.editor.getDoc().setSelections(e), t && this.editor.focus()
                }, e.prototype.apply = function(e) {
                    e && e.selection && e.selection.ranges && this.delegateSelection(e.selection.ranges, e.selection.focus || !1), e && e.cursor && this.delegateCursor(e.cursor, e.autoScroll || !1, this.editor.getOption("autofocus") || !1), e && e.scroll && this.delegateScroll(e.scroll)
                }, e.prototype.applyNext = function(e, t, o) {
                    e && e.selection && e.selection.ranges && t && t.selection && t.selection.ranges && !u.equals(e.selection.ranges, t.selection.ranges) && this.delegateSelection(t.selection.ranges, t.selection.focus || !1), e && e.cursor && t && t.cursor && !u.equals(e.cursor, t.cursor) && this.delegateCursor(o.cursor || t.cursor, t.autoScroll || !1, t.autoCursor || !1), e && e.scroll && t && t.scroll && !u.equals(e.scroll, t.scroll) && this.delegateScroll(t.scroll)
                }, e.prototype.applyUserDefined = function(e, t) {
                    t && t.cursor && this.delegateCursor(t.cursor, e.autoScroll || !1, this.editor.getOption("autofocus") || !1)
                }, e.prototype.wire = function(e) {
                    var t = this;
                    Object.keys(e || {}).filter((function(e) {
                        return /^on/.test(e)
                    })).forEach((function(e) {
                        switch (e) {
                            case "onBlur":
                                t.editor.on("blur", (function(e, o) {
                                    t.props.onBlur(t.editor, o)
                                }));
                                break;
                            case "onContextMenu":
                                t.editor.on("contextmenu", (function(e, o) {
                                    t.props.onContextMenu(t.editor, o)
                                }));
                                break;
                            case "onCopy":
                                t.editor.on("copy", (function(e, o) {
                                    t.props.onCopy(t.editor, o)
                                }));
                                break;
                            case "onCursor":
                                t.editor.on("cursorActivity", (function(e) {
                                    t.props.onCursor(t.editor, t.editor.getDoc().getCursor())
                                }));
                                break;
                            case "onCursorActivity":
                                t.editor.on("cursorActivity", (function(e) {
                                    t.props.onCursorActivity(t.editor)
                                }));
                                break;
                            case "onCut":
                                t.editor.on("cut", (function(e, o) {
                                    t.props.onCut(t.editor, o)
                                }));
                                break;
                            case "onDblClick":
                                t.editor.on("dblclick", (function(e, o) {
                                    t.props.onDblClick(t.editor, o)
                                }));
                                break;
                            case "onDragEnter":
                                t.editor.on("dragenter", (function(e, o) {
                                    t.props.onDragEnter(t.editor, o)
                                }));
                                break;
                            case "onDragLeave":
                                t.editor.on("dragleave", (function(e, o) {
                                    t.props.onDragLeave(t.editor, o)
                                }));
                                break;
                            case "onDragOver":
                                t.editor.on("dragover", (function(e, o) {
                                    t.props.onDragOver(t.editor, o)
                                }));
                                break;
                            case "onDragStart":
                                t.editor.on("dragstart", (function(e, o) {
                                    t.props.onDragStart(t.editor, o)
                                }));
                                break;
                            case "onDrop":
                                t.editor.on("drop", (function(e, o) {
                                    t.props.onDrop(t.editor, o)
                                }));
                                break;
                            case "onFocus":
                                t.editor.on("focus", (function(e, o) {
                                    t.props.onFocus(t.editor, o)
                                }));
                                break;
                            case "onGutterClick":
                                t.editor.on("gutterClick", (function(e, o, a, n) {
                                    t.props.onGutterClick(t.editor, o, a, n)
                                }));
                                break;
                            case "onInputRead":
                                t.editor.on("inputRead", (function(e, o) {
                                    t.props.onInputRead(t.editor, o)
                                }));
                                break;
                            case "onKeyDown":
                                t.editor.on("keydown", (function(e, o) {
                                    t.props.onKeyDown(t.editor, o)
                                }));
                                break;
                            case "onKeyHandled":
                                t.editor.on("keyHandled", (function(e, o, a) {
                                    t.props.onKeyHandled(t.editor, o, a)
                                }));
                                break;
                            case "onKeyPress":
                                t.editor.on("keypress", (function(e, o) {
                                    t.props.onKeyPress(t.editor, o)
                                }));
                                break;
                            case "onKeyUp":
                                t.editor.on("keyup", (function(e, o) {
                                    t.props.onKeyUp(t.editor, o)
                                }));
                                break;
                            case "onMouseDown":
                                t.editor.on("mousedown", (function(e, o) {
                                    t.props.onMouseDown(t.editor, o)
                                }));
                                break;
                            case "onPaste":
                                t.editor.on("paste", (function(e, o) {
                                    t.props.onPaste(t.editor, o)
                                }));
                                break;
                            case "onRenderLine":
                                t.editor.on("renderLine", (function(e, o, a) {
                                    t.props.onRenderLine(t.editor, o, a)
                                }));
                                break;
                            case "onScroll":
                                t.editor.on("scroll", (function(e) {
                                    t.props.onScroll(t.editor, t.editor.getScrollInfo())
                                }));
                                break;
                            case "onSelection":
                                t.editor.on("beforeSelectionChange", (function(e, o) {
                                    t.props.onSelection(t.editor, o)
                                }));
                                break;
                            case "onTouchStart":
                                t.editor.on("touchstart", (function(e, o) {
                                    t.props.onTouchStart(t.editor, o)
                                }));
                                break;
                            case "onUpdate":
                                t.editor.on("update", (function(e) {
                                    t.props.onUpdate(t.editor)
                                }));
                                break;
                            case "onViewportChange":
                                t.editor.on("viewportChange", (function(e, o, a) {
                                    t.props.onViewportChange(t.editor, o, a)
                                }))
                        }
                    }))
                }, e
            }(),
            m = function(e) {
                function t(t) {
                    var o = e.call(this, t) || this;
                    return p || (o.applied = !1, o.appliedNext = !1, o.appliedUserDefined = !1, o.deferred = null, o.emulating = !1, o.hydrated = !1, o.initCb = function() {
                        o.props.editorDidConfigure && o.props.editorDidConfigure(o.editor)
                    }, o.mounted = !1), o
                }
                return r(t, e), t.prototype.hydrate = function(e) {
                    var t = this,
                        o = e && e.options ? e.options : {},
                        n = a({}, i.defaults, this.editor.options, o);
                    Object.keys(n).some((function(e) {
                        return t.editor.getOption(e) !== n[e]
                    })) && Object.keys(n).forEach((function(e) {
                        o.hasOwnProperty(e) && t.editor.getOption(e) !== n[e] && (t.editor.setOption(e, n[e]), t.mirror.setOption(e, n[e]))
                    })), this.hydrated || (this.deferred ? this.resolveChange(e.value) : this.initChange(e.value || "")), this.hydrated = !0
                }, t.prototype.initChange = function(e) {
                    this.emulating = !0;
                    var t = this.editor.getDoc(),
                        o = t.lastLine(),
                        a = t.getLine(t.lastLine()).length;
                    t.replaceRange(e || "", {
                        line: 0,
                        ch: 0
                    }, {
                        line: o,
                        ch: a
                    }), this.mirror.setValue(e), t.clearHistory(), this.mirror.clearHistory(), this.emulating = !1
                }, t.prototype.resolveChange = function(e) {
                    this.emulating = !0;
                    var t = this.editor.getDoc();
                    if ("undo" === this.deferred.origin ? t.undo() : "redo" === this.deferred.origin ? t.redo() : t.replaceRange(this.deferred.text, this.deferred.from, this.deferred.to, this.deferred.origin), e && e !== t.getValue()) {
                        var o = t.getCursor();
                        t.setValue(e), t.setCursor(o)
                    }
                    this.emulating = !1, this.deferred = null
                }, t.prototype.mirrorChange = function(e) {
                    var t = this.editor.getDoc();
                    return "undo" === e.origin ? (t.setHistory(this.mirror.getHistory()), this.mirror.undo()) : "redo" === e.origin ? (t.setHistory(this.mirror.getHistory()), this.mirror.redo()) : this.mirror.replaceRange(e.text, e.from, e.to, e.origin), this.mirror.getValue()
                }, t.prototype.componentDidMount = function() {
                    var e = this;
                    p || (this.props.defineMode && this.props.defineMode.name && this.props.defineMode.fn && i.defineMode(this.props.defineMode.name, this.props.defineMode.fn), this.editor = i(this.ref, this.props.options), this.shared = new c(this.editor, this.props), this.mirror = i((function() {}), this.props.options), this.editor.on("electricInput", (function() {
                        e.mirror.setHistory(e.editor.getDoc().getHistory())
                    })), this.editor.on("cursorActivity", (function() {
                        e.mirror.setCursor(e.editor.getDoc().getCursor())
                    })), this.editor.on("beforeChange", (function(t, o) {
                        if (!e.emulating) {
                            o.cancel(), e.deferred = o;
                            var a = e.mirrorChange(e.deferred);
                            e.props.onBeforeChange && e.props.onBeforeChange(e.editor, e.deferred, a)
                        }
                    })), this.editor.on("change", (function(t, o) {
                        e.mounted && e.props.onChange && e.props.onChange(e.editor, o, e.editor.getValue())
                    })), this.hydrate(this.props), this.shared.apply(this.props), this.applied = !0, this.mounted = !0, this.shared.wire(this.props), this.editor.getOption("autofocus") && this.editor.focus(), this.props.editorDidMount && this.props.editorDidMount(this.editor, this.editor.getValue(), this.initCb))
                }, t.prototype.componentDidUpdate = function(e) {
                    if (!p) {
                        var t = {
                            cursor: null
                        };
                        this.props.value !== e.value && (this.hydrated = !1), this.props.autoCursor || void 0 === this.props.autoCursor || (t.cursor = this.editor.getDoc().getCursor()), this.hydrate(this.props), this.appliedNext || (this.shared.applyNext(e, this.props, t), this.appliedNext = !0), this.shared.applyUserDefined(e, t), this.appliedUserDefined = !0
                    }
                }, t.prototype.componentWillUnmount = function() {
                    p || this.props.editorWillUnmount && this.props.editorWillUnmount(i)
                }, t.prototype.shouldComponentUpdate = function(e, t) {
                    return !p
                }, t.prototype.render = function() {
                    var e = this;
                    if (p) return null;
                    var t = this.props.className ? "react-codemirror2 " + this.props.className : "react-codemirror2";
                    return l.createElement("div", {
                        className: t,
                        ref: function(t) {
                            return e.ref = t
                        }
                    })
                }, t
            }(l.Component);
        t.Controlled = m;
        var d = function(e) {
            function t(t) {
                var o = e.call(this, t) || this;
                return p || (o.applied = !1, o.appliedUserDefined = !1, o.continueChange = !1, o.detached = !1, o.hydrated = !1, o.initCb = function() {
                    o.props.editorDidConfigure && o.props.editorDidConfigure(o.editor)
                }, o.mounted = !1, o.onBeforeChangeCb = function() {
                    o.continueChange = !0
                }), o
            }
            return r(t, e), t.prototype.hydrate = function(e) {
                var t = this,
                    o = e && e.options ? e.options : {},
                    n = a({}, i.defaults, this.editor.options, o);
                if (Object.keys(n).some((function(e) {
                        return t.editor.getOption(e) !== n[e]
                    })) && Object.keys(n).forEach((function(e) {
                        o.hasOwnProperty(e) && t.editor.getOption(e) !== n[e] && t.editor.setOption(e, n[e])
                    })), !this.hydrated) {
                    var s = this.editor.getDoc(),
                        r = s.lastLine(),
                        l = s.getLine(s.lastLine()).length;
                    s.replaceRange(e.value || "", {
                        line: 0,
                        ch: 0
                    }, {
                        line: r,
                        ch: l
                    })
                }
                this.hydrated = !0
            }, t.prototype.componentDidMount = function() {
                var e = this;
                p || (this.detached = !0 === this.props.detach, this.props.defineMode && this.props.defineMode.name && this.props.defineMode.fn && i.defineMode(this.props.defineMode.name, this.props.defineMode.fn), this.editor = i(this.ref, this.props.options), this.shared = new c(this.editor, this.props), this.editor.on("beforeChange", (function(t, o) {
                    e.props.onBeforeChange && e.props.onBeforeChange(e.editor, o, e.editor.getValue(), e.onBeforeChangeCb)
                })), this.editor.on("change", (function(t, o) {
                    e.mounted && e.props.onChange && (e.props.onBeforeChange ? e.continueChange && e.props.onChange(e.editor, o, e.editor.getValue()) : e.props.onChange(e.editor, o, e.editor.getValue()))
                })), this.hydrate(this.props), this.shared.apply(this.props), this.applied = !0, this.mounted = !0, this.shared.wire(this.props), this.editor.getDoc().clearHistory(), this.props.editorDidMount && this.props.editorDidMount(this.editor, this.editor.getValue(), this.initCb))
            }, t.prototype.componentDidUpdate = function(e) {
                if (this.detached && !1 === this.props.detach && (this.detached = !1, e.editorDidAttach && e.editorDidAttach(this.editor)), this.detached || !0 !== this.props.detach || (this.detached = !0, e.editorDidDetach && e.editorDidDetach(this.editor)), !p && !this.detached) {
                    var t = {
                        cursor: null
                    };
                    this.props.value !== e.value && (this.hydrated = !1, this.applied = !1, this.appliedUserDefined = !1), e.autoCursor || void 0 === e.autoCursor || (t.cursor = this.editor.getDoc().getCursor()), this.hydrate(this.props), this.applied || (this.shared.apply(e), this.applied = !0), this.appliedUserDefined || (this.shared.applyUserDefined(e, t), this.appliedUserDefined = !0)
                }
            }, t.prototype.componentWillUnmount = function() {
                p || this.props.editorWillUnmount && this.props.editorWillUnmount(i)
            }, t.prototype.shouldComponentUpdate = function(e, t) {
                var o = !0;
                return p && (o = !1), this.detached && e.detach && (o = !1), o
            }, t.prototype.render = function() {
                var e = this;
                if (p) return null;
                var t = this.props.className ? "react-codemirror2 " + this.props.className : "react-codemirror2";
                return l.createElement("div", {
                    className: t,
                    ref: function(t) {
                        return e.ref = t
                    }
                })
            }, t
        }(l.Component);
        t.UnControlled = d
    }).call(this, o(4))
}, function(e, t, o) {
    "use strict";
    e.exports = e => {
        const t = typeof e;
        return null !== e && ("object" === t || "function" === t)
    }
}, function(e, t) {
    var o;
    o = function() {
        return this
    }();
    try {
        o = o || new Function("return this")()
    } catch (e) {
        "object" == typeof window && (o = window)
    }
    e.exports = o
}, function(e, t) {
    e.exports = React
}, function(e, t) {
    e.exports = wp.CodeMirror
}, function(e, t, o) {
    "use strict";
    o.r(t);
    const {
        createContext: a,
        useState: n
    } = wp.element, s = a(), r = ({
        children: e,
        value: t
    }) => {
        const [o, a] = n(t);
        return React.createElement(s.Provider, {
            value: {
                settings: o,
                updateSettings: e => a(t => ({
                    ...t,
                    ...e
                }))
            }
        }, e)
    }, {
        __: i
    } = wp.i18n;
    var l = {
            name: "",
            title: "",
            function_name: "my_acf_init_blocks",
            text_domain: jQuery(".act_themes").val(),
            description: "",
            align: "wide",
            align_text: "left",
            align_content: "left",
            category: "",
            mode: "preview",
            icon: "",
            keywords: [],
            supports: [],
            post_types: ["post", "page"],
            render_callback: "my_acf_block_render_callback",
            enqueue_style: true,
            enqueue_script: false,
            
        },
        p = o(0),
        u = o.n(p);
    const {
        useContext: c
    } = wp.element;
    var m = ({
            name: e,
            options: t,
            description: o
        }) => {
            const {
                settings: a,
                updateSettings: n
            } = c(s), r = u.a.get(a, e, []), i = t => {
                const {
                    value: o,
                    checked: a
                } = t.target;
                let s = [...r];
                a ? s.push(o) : s = s.filter(e => e !== o), n({
                    [e]: s
                })
            };
            return React.createElement("div", {
                className: "mo-cpt-field"
            }, React.createElement("div", {
                className: "mo-cpt-input"
            }, o && React.createElement("div", {
                className: "mo-cpt-description"
            }, o), React.createElement("ul", {
                className: "mo-cpt-input-list"
            }, Object.entries(t).map(([e, t]) => React.createElement("li", {
                key: e
            }, React.createElement("label", null, React.createElement("input", {
                type: "checkbox",
                value: e,
                checked: r.includes(e),
                onChange: i
            }), t))))))
        },
        d = o(1),
        b = o.n(d);
    var h = ({
        label: e,
        name: t,
        description: o,
        update: a,
        checked: n
    }) => React.createElement("div", {
        className: "mo-cpt-field"
    }, e && React.createElement("label", {
        className: "mo-cpt-label",
        htmlFor: t
    }, e), React.createElement("div", {
        className: "mo-cpt-input"
    }, o ? React.createElement("label", {
        className: "mo-cpt-description"
    }, React.createElement("input", {
        type: "checkbox",
        id: t,
        name: t,
        checked: n,
        onChange: a
    }), " ", o) : React.createElement("input", {
        type: "checkbox",
        id: t,
        name: t,
        checked: n,
        onChange: a
    })));
    const {
        Dashicon: y
    } = wp.components;
    var f = ({
        label: e,
        name: t,
        update: o,
        value: a
    }) => React.createElement("div", {
        className: "mo-cpt-field mo-cpt-field--radio"
    }, React.createElement("label", {
        className: "mo-cpt-label"
    }, e), React.createElement("div", {
        className: "mo-cpt-input"
    }, MOB.icons.map(e => React.createElement("label", {
        key: e,
        className: "mo-cpt-choice mo-cpt-icon"
    }, React.createElement("input", {
        type: "radio",
        name: t,
        value: e,
        checked: e === a,
        onChange: o
    }), React.createElement(y, {
        icon: e
    })))));
    const {
        useEffect: g
    } = wp.element;
    var _ = ({
        label: e,
        name: t,
        value: o,
        update: a,
        description: n = "",
        required: s = !1
    }) => (g(() => {
        if (!["title"].includes(t)) return;
        a({
            target: {
                name: t,
                value: o
            }
        })
    }, []), React.createElement("div", {
        className: "mo-cpt-field"
    }, React.createElement("label", {
        className: "mo-cpt-label",
        htmlFor: t
    }, e, s && React.createElement("span", {
        className: "mo-cpt-required"
    }, "*")), React.createElement("div", {
        className: "mo-cpt-input"
    }, React.createElement("input", {
        type: "text",
        required: s,
        id: t,
        name: t,
        value: o,
        onChange: a
    }), n && React.createElement("div", {
        className: "mo-cpt-description"
    }, n))));
    var v = ({
        label: e,
        name: t,
        update: o,
        options: a,
        value: n
    }) => React.createElement("div", {
        className: "mo-cpt-field mo-cpt-field--radio"
    }, React.createElement("label", {
        className: "mo-cpt-label"
    }, e), React.createElement("div", {
        className: "mo-cpt-input"
    }, a.map(e => React.createElement("label", {
        key: e.value,
        className: "mo-cpt-choice"
    }, React.createElement("input", {
        type: "radio",
        name: t,
        value: e.value,
        checked: e.value === n,
        onChange: o
    }), e.label))));
    var w = ({
        label: e,
        name: t,
        update: o,
        description: a = "",
        options: n,
        value: s
    }) => React.createElement("div", {
        className: "mo-cpt-field"
    }, React.createElement("label", {
        className: "mo-cpt-label",
        htmlFor: t
    }, e), React.createElement("div", {
        className: "mo-cpt-input"
    }, React.createElement("select", {
        id: t,
        name: t,
        value: s,
        onChange: o
    }, n.map(e => React.createElement("option", {
        key: e.value,
        value: e.value
    }, e.label))), a && React.createElement("div", {
        className: "mo-cpt-description"
    }, a)));
    var x = ({
        label: e,
        name: t,
        placeholder: o,
        value: a,
        update: n,
        description: s = "",
        required: r = !1
    }) => React.createElement("div", {
        className: "mo-cpt-field"
    }, React.createElement("label", {
        className: "mo-cpt-label",
        htmlFor: t
    }, e, r && React.createElement("span", {
        className: "mo-cpt-required"
    }, "*")), React.createElement("div", {
        className: "mo-cpt-input"
    }, React.createElement("textarea", {
        id: t,
        name: t,
        placeholder: o,
        value: a,
        onChange: n
    }), s && React.createElement("div", {
        className: "mo-cpt-description"
    }, s)));
    const {
        useContext: C
    } = wp.element;
    var E = ({
        field: e,
        autoFills: t = []
    }) => {
        const {
            settings: o,
            updateSettings: a
        } = C(s), n = e => {
            const n = e.target.name;
            let s = "checkbox" === e.target.type ? e.target.checked : e.target.value;
            s = (e => ("true" === e ? e = !0 : "false" === e && (e = !1), e))(s), s = (e => isNaN(parseInt(e)) ? e : parseInt(e))(s);
            let r = {
                ...o
            };
            u.a.set(r, n, s), ((e, o, a) => {
                const n = o.replace("labels.", "");
                t.forEach(t => {
                    let o;
                    var s;
                    o = "name" === t.name ? b()(a, {
                        lower: !0
                    }) : (s = t.default.replace(`%${n}%`, t.default.split(" ").length > 2 ? a.toLowerCase() : a)).length ? s[0].toUpperCase() + s.slice(1) : s, u.a.set(e, t.name, o)
                })
            })(r, n, s), a(r)
        }, r = u.a.get(o, e.name) || e.default || "";
        switch (e.type) {
            case "text":
                return React.createElement(_, {
                    label: e.label,
                    name: e.name,
                    value: r,
                    description: e.description,
                    required: e.required,
                    update: n
                });
            case "textarea":
                return React.createElement(x, {
                    label: e.label,
                    name: e.name,
                    placeholder: e.placeholder,
                    value: r,
                    description: e.description,
                    update: n
                });
            case "checkbox":
                return React.createElement(h, {
                    label: e.label,
                    name: e.name,
                    description: e.description,
                    checked: r,
                    update: n
                });
            case "radio":
                return React.createElement(v, {
                    label: e.label,
                    name: e.name,
                    options: e.options,
                    value: r,
                    update: n
                });
            case "icon":
                return React.createElement(f, {
                    label: e.label,
                    name: e.name,
                    value: r,
                    update: n
                });
            case "select":
                return React.createElement(w, {
                    label: e.label,
                    name: e.name,
                    description: e.description,
                    options: e.options,
                    value: r,
                    update: n
                })
        }
    };
    const {
        __: k
    } = wp.i18n, R = [{
            type: "text",
            name: "title",
            label: k("Title", "acf-block-generator"),
            required: !0
        }, {
            type: "text",
            name: "name",
            label: k("Name", "acf-block-generator"),
            required: !0,
            updateFrom: "title"
        }], S = [{
            type: "text",
            name: "function_name",
            label: k("Function name", "acf-block-generator")
        }, {
            type: "text",
            name: "text_domain",
            label: k("Text domain", "acf-block-generator")
        }], A = [{
            name: "category",
            value: "common",
            label: k("Common", "acf-block-generator")
        }, {
            name: "category",
            value: "formatting",
            label: k("Formatting", "acf-block-generator")
        }, {
            name: "category",
            value: "layout",
            label: k("Layout", "acf-block-generator")
        }, {
            name: "category",
            value: "widgets",
            label: k("Widgets", "acf-block-generator")
        }, {
            name: "category",
            value: "embed",
            label: k("Embed", "acf-block-generator")
        }], Mo = [{
            name: "mode",
            value: "preview",
            label: k("Preview", "acf-block-generator"),
        }, {
            name: "mode",
            value: "auto",
            label: k("Auto", "acf-block-generator")
        }, {
            name: "mode",
            value: "edit",
            label: k("Edit", "acf-block-generator")
        }], Al = [{
            name: "align",
            value: "left",
            label: k("Left", "acf-block-generator")
        }, {
            name: "align",
            value: "center",
            label: k("Center", "acf-block-generator")
        }, {
            name: "align",
            value: "right",
            label: k("Right", "acf-block-generator")
        }, {
            name: "align",
            value: "wide",
            label: k("Wide", "acf-block-generator")
        }, {
            name: "align",
            value: "full",
            label: k("Full", "acf-block-generator")
        }], At = [{
            name: "align_text",
            value: "left",
            label: k("Left", "acf-block-generator")
        }, {
            name: "align_text",
            value: "center",
            label: k("Center", "acf-block-generator")
        }, {
            name: "align_text",
            value: "right",
            label: k("Right", "acf-block-generator")
        }], Ac = [{
            name: "align_content",
            value: "left",
            label: k("Left", "acf-block-generator")
        }, {
            name: "align_content",
            value: "center",
            label: k("Center", "acf-block-generator")
        }, {
            name: "align_content",
            value: "right",
            label: k("Right", "acf-block-generator")
        }], St = [{
            type: "select",
            name: "align",
            label: k("Block Alignment", "acf-block-generator"),
            description: k("The default block alignment.", "acf-block-generator"),
            options: Al,
            default: "center"
        }, {
            type: "select",
            name: "align_text",
            label: k("Text Alignment", "acf-block-generator"),
            description: k("The default text alignment.", "acf-block-generator"),
            options: At,
            default: "left"
        }, {
            type: "select",
            name: "align_content",
            label: k("Content Alignment", "acf-block-generator"),
            description: k("The default content alignment.", "acf-block-generator"),
            options: Ac,
            default: "left"
        }, {
            type: "checkbox",
            name: "enqueue_style",
            label: k("Enqueue Style", "acf-block-generator"),
            description: k("If checked a CSS file will be created in the Block folder, you can then edit this to apply style.", "acf-block-generator")
        }, {
            type: "checkbox",
            name: "enqueue_script",
            label: k("Enqueue Script", "acf-block-generator"),
            description: k("If checked a JS file will be created in the Block folder, you can then edit this to apply scripts.", "acf-block-generator")
        }], P = [{
            type: "textarea",
            name: "description",
            label: k("Description", "acf-block-generator"),
            placeholder: k("A short descriptive summary of what the post type is", "acf-block-generator")
        }, {
            type: "textarea",
            name: "keywords",
            label: k("Keywords", "acf-block-generator"),
            placeholder: k("A comma seperated list of keywords which you want the Block to be found on", "acf-block-generator")
        }, {
            type: "select",
            name: "category",
            label: k("Category", "acf-block-generator"),
            options: A,
            default: "common"
        }, {
            type: "select",
            name: "mode",
            label: k("Mode", "acf-block-generator"),
            description: k("In Auto Mode the Preview is shown by default but changes to edit form when block is selected. In Preview Mode the Edit form appears in sidebar when block is selected. In Edit Mode Edit form is always shown.", "acf-block-generator"),
            options: Mo,
            default: "preview"
        }, {
            type: "icon",
            name: "icon",
            label: k("Icon", "acf-block-generator")
        }], N = [{
            type: "checkbox",
            name: "Mode",
            label: k("Mode", "acf-block-generator"),
            description: k("This property allows the user to toggle between edit and preview modes via a button.", "acf-block-generator")
        },{
            type: "checkbox",
            name: "Align",
            label: k("Align", "acf-block-generator"),
            description: k("This property enables a toolbar button to control the block’s alignment.", "acf-block-generator"),
            checked: true
        },{
            type: "checkbox",
            name: "Align_text",
            label: k("Align Text", "acf-block-generator"),
            description: k("This property enables a toolbar button to control the block’s text alignment.", "acf-block-generator")
        },{
            type: "checkbox",
            name: "Align_content",
            label: k("Align content", "acf-block-generator"),
            description: k("This property enables a toolbar button to control the block’s inner content alignment.", "acf-block-generator")
        },{
            type: "checkbox",
            name: "Multiple",
            label: k("Multiple", "acf-block-generator"),
            description: k("This property allows the block to be added multiple times.", "acf-block-generator"),
              checked: true
        }, {
            type: "checkbox",
            name: "Jsx",
            label: k("JSX", "acf-block-generator"),
            description: k("This property allows you to use the <InnerBlocks /> component in your PHP template.", "acf-block-generator")
        }], F = (e, t) => {
            return " ".repeat((o = e, Math.max.apply(null, Object.keys(o).map(e => e.length)) - t.length));
            var o
        },
        

        M = (e, t) => `'${t}'${F(e,t)} => '${u.a.get(e,t,"")}'`,
        $ = (e, t) => `'${t}'${F(e,t)} => esc_html__( '${u.a.get(e,t,"")}', '${e.text_domain}' )`,
        rc = (e, t) => `'${t}'${F(e,t)} => 'my_acf_block_render_callback'`,
        //ke = (e, t) => `'${t}'${F(e,t)} => ${u.a.get(e,t,"").length?`['${u.a.get(e,t,"").split(',').join('\',\'')}']`:"[]"}`,
        ke = (e, t) => `'${t}'${F(e,t)} => ${u.a.get(e,t,"").length?`['${e.keywords.join('\',\'')}']`:"[]"}`,
        j = (e, t) => `'${t}'${F(e,t)} => ${u.a.get(e,t,[]).length?`['${u.a.get(e,t,[]).join("', '")}']`:""}`,
        
        Es = (e, t) => `'${t}'${F(e,t)} => ${u.a.get(e,t,"") != " "?`'${MOB.template_direcoty_uri}/template-parts/blocks/${e.name}/${e.name}.css'`:""}`,
        Ec = (e, t) => `'${t}'${F(e,t)} => ${u.a.get(e,t,"") != " "?`'${MOB.template_direcoty_uri}/template-parts/blocks/${e.name}/${e.name}.js'`:""}`,
        sc = (e, t) => `'${t.toLowerCase()}'${F(e,t)} => ${u.a.get(e,t,"") == true?true:false}`,
        I = (e, t) => {
            let o = u.a.get(e, t);
            return ["", void 0].includes(o) && (o = "''"), `'${t}'${F(e,t)} => ${o}`
        };

        
    var T = e => `<?php
add_action( 'acf/init', '${e.function_name}' );

function ${e.function_name}() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {
        $args = [
            ${$(e,"title")},
            ${$(e,"description")},
            ${M(e,"name")},
            ${M(e,"align")},
            ${M(e,"align_text")},
            ${M(e,"align_content")},
            ${M(e,"category")},
            ${M(e,"mode")},
            ${M(e,"icon")},
            ${ke(e,"keywords")},
            'supports' \xa0\xa0\xa0\xa0\xa0\xa0\xa0=> Array
                    (
                                    ${sc(e,"Mode")},  
                                    ${sc(e,"Jsx")},  
                                    ${sc(e,"Align")},  
                                    ${sc(e,"Align_text")},  
                                    ${sc(e,"Align_content")},  
                                    ${sc(e,"Multiple")},  
                    ),
            ${j(e,"post_types")},
            ${rc(e,"render_callback")},
            ${Es(e,"enqueue_style")},
            ${Ec(e,"enqueue_script")},
        ];

        // Register the block.
        acf_register_block_type( $args );
    }
}`,
        L = o(2);
    const {
        useContext: q
    } = wp.element, {
        __: H
    } = wp.i18n, {
        ClipboardButton: W
    } = wp.components, {
        withState: V
    } = wp.compose;
    var B = () => {
        const {
            settings: e
        } = q(s),
            t = V({
                hasCopied: !1
            })(({
                hasCopied: t,
                setState: o
            }) => React.createElement(W, {
                    className: "button",
                    text: T(e),
                    onCopy: () => o({
                        hasCopied: !0
                    }),
                    onFinishCopy: () => o({
                        hasCopied: !1
                    })
                },
                H(t ? "Copied!" : "Copy", "acf-block-generator")));
        return React.createElement("div", {
            className: "mo-cpt-result ok"
        }, React.createElement("p", null, H("Copy and paste the following code into your theme's functions.php file.", "acf-block-generator")), React.createElement("div", {
            className: "mo-cpt-result__body"
        }, React.createElement(L.UnControlled, {
            value: T(e),
            options: {
                mode: "php",
                lineNumbers: !0
            }
        }), React.createElement(t, null)))
    };
    const {
        useContext: Y
    } = wp.element, {
        TabPanel: z
    } = wp.components, {
        __: G
    } = wp.i18n, J = [{
        name: "general",
        title: G("General", "acf-block-generator")
    }, {
        name: "style",
        title: G("Style", "acf-block-generator")
    }, {
        name: "advanced",
        title: G("Advanced", "acf-block-generator")
    }, {
        name: "supports",
        title: G("Supports", "acf-block-generator")
    }, {
        name: "post_types",
        title: G("Post Types", "acf-block-generator")
    }, {
        name: "code",
        title: G("Get PHP Code", "acf-block-generator"),
        className: "mo-cpt-code button button-small"
    }];
    let K = [...R];
    K.push({
        name: "name",
        default: "%name%",
        updateFrom: "title"
    });
    const Z = {
        general: R.map((e, t) => React.createElement(E, {
            key: t,
            field: e,
            autoFills: K.filter(t => t.updateFrom === e.name)
        })),
        style: St.map((e, t) => React.createElement(E, {
            key: t,
            field: e
        })),
        advanced: P.map((e, t) => React.createElement(E, {
            key: t,
            field: e
        })),
        supports: N.map((e, t) => React.createElement(E, {
            key: t,
            field: e
        })),
        post_types: React.createElement(m, {
            name: "post_types",
            options: MOB.post_types,
            description: G("Post Types on which this block can be used:", "acf-block-generator")
        }),
        code: React.createElement(React.Fragment, null, S.map((e, t) => React.createElement(E, {
            key: t,
            field: e
        })), React.createElement(B, null))
    };

    var Q = () => {
        const {
            settings: e
        } = Y(s);

        if(e.Mode == undefined){
          e.Mode = false;  
        }

         if(e.Jsx == undefined){
          e.Jsx = false;  
        }

         if(e.Align == undefined){
          e.Align = true;  
        }

         if(e.Align_text == undefined){
          e.Align_text = false;  
        }

         if(e.Align_content == undefined){
          e.Align_content = false;  
        }

         if(e.Multiple == undefined){
          e.Multiple = true;  
        }

        e.supports = {mode: e.Mode, jsx:e.Jsx, align:e.Align, align_text:e.Align_text, align_content:e.Align_content, multiples:e.Multiple};
        //e.supports = "['mode' => "+e.Mode+", 'jsx' => "+e.Jsx+", 'align' => "+e.Align+", 'align_text' => "+e.Align_text+", 'align_content' => "+e.Align_content+", 'multiples' => "+e.Multiple+"]";
      //  console.log(e.enqueue_style);
        //e.supports = {mode: e.Mode, jsx: true, align: true, align_text: true, align_content: true, multiples: true};

        
        e.enqueue_style = MOB.template_direcoty_uri+'/template-parts/blocks/'+e.name+'/'+e.name+'.css';    
        
        if(e.enqueue_script == true){
        e.enqueue_script = MOB.template_direcoty_uri+'/template-parts/blocks/'+e.name+'/'+e.name+'.js';
        }

    

        if(e.keywords && !Array.isArray(e.keywords)){
            e.keywords = e.keywords.split(',');
        }else{
            if(!Array.isArray(e.keywords)){
                e.keywords = [];
            }
        }     
          
        return React.createElement(
            React.Fragment, null, 
            React.createElement(z, {
                className: "mo-cpt-tabs",
                tabs: J
            }, e => Z[e.name]), 
            React.createElement("input", {
                type: "hidden",
                name: "post_title",
                value: e.title
            }),
            React.createElement("input", {
                type: "hidden",
                name: "content",
                value: JSON.stringify(e)
            })
        )
    };
    const {
        render: X
    } = wp.element, ee = () => React.createElement(r, {
        value: MOB.settings ? MOB.settings : l
    }, React.createElement(Q, null));
    X(React.createElement(ee, null), document.getElementById("root"))
}]);