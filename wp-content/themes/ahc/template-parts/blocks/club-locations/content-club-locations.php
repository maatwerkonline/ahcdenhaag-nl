<?php
/**
 * Block Name: Club locations
 * This is the template that displays the block club-locations.
 *
 * @author Maatje van Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Club locations -->
	<?php
	$image = get_field('image');
	$club = get_field('club_name');
	$adres = get_field('adres');
	$maps = get_field('maps_link');
	$email = get_field('email');
	$phone = get_field('phone');
	?>
	<div class="col-4 d-flex">
		<div class="my-auto">
			<img class="club-locations__img" src="<?php echo $image; ?>" alt="Google maps image">
		</div>
	</div>
	<div class="col-8 d-flex">
		<div class="my-auto">
			<h6 class=""><?php echo $club; ?></h6>
			<a class="club-locations__link" href="<?php echo $maps; ?>" class="mb-0"><i class="ahc ahc-pin"></i><?php echo $adres; ?></a>
			<a class="club-locations__link" href="mailto:<?php echo $email; ?>" class="mb-0"><i class="ahc ahc-mail"></i><?php echo $email; ?></a>
			<a class="club-locations__link" href="tel:<?php echo $phone; ?>" class="mb-0"><i class="ahc ahc-phone"></i><?php echo $phone; ?></a>
			<a class="club-locations__button pt-1" href="<?php echo $maps; ?>"><h6 class="mb-0"><?php _e("Plan your route", "maatwerkonline"); ?> <i class="ahc ahc-arrow-right pl-2"></i></h6></a>
		</div>
	</div>
</div>