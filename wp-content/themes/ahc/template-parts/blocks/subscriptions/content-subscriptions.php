<?php
/**
 * Block Name: Subscriptions
 * This is the template that displays the block subscriptions.
 *
 * @author Maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Subscriptions block -->
	<?php
	$background_image = get_field('background_image');
	$subscription_link = get_field('all_subscriptions_link');

	$title_1 = get_field('title_1');
	$price_1 = get_field('price_1');
	$price_description_1 = get_field('price_description_1');
	$list_items_1 = get_field('list_items_1');
	$button_link_1_1 = get_field('button_link_1_1');
	$button_link_2_1 = get_field('button_link_2_1');

	$title_2 = get_field('title_2');
	$price_2 = get_field('price_2');
	$price_description_2 = get_field('price_description_2');
	$list_items_2 = get_field('list_items_2');
	$button_link_1_2 = get_field('button_link_1_2');
	$button_link_2_2 = get_field('button_link_2_2');
	$usp_image_2 = get_field('usp_image_2');

	$title_3 = get_field('title_3');
	$price_3 = get_field('price_3');
	$price_description_3 = get_field('price_description_3');
	$list_items_3 = get_field('list_items_3');
	$button_link_1_3 = get_field('button_link_1_3');
	$button_link_2_3 = get_field('button_link_2_3');
	$usp_image_3 = get_field('usp_image_3');
	?>
	<div class="wp-bootstrap-blocks-row row subscription-row">

		<div class="col-12 col-md-4 py-3 py-md-0">
			<div class="subscription">
				<h3 class="capital"><?php echo $title_1; ?></h3>
				<div class="d-flex price pb-4">
					<h3 class="pr-2 mb-0"><?php echo $price_1; ?></h3>
					<p class="special-margin mb-0"><?php echo $price_description_1; ?></p>
				</div>
				<ul class="subscription__list pb-3">
					<?php foreach ($list_items_1 as $list_item) : ?>
						<li class="<?php if ( $list_item['included'] ) { echo 'included'; } else { echo 'excluded'; } ?>"><?php echo $list_item['content']; ?> <?php if ($list_item['tooltip']) { ?> <i class="ahc ahc-info"><div class="list-description"><p class="mb-0"><?php echo $list_item['tooltip_content']; ?></p></div></i> <?php } ?></li>
					<?php endforeach; ?>
				</ul>
				<div class="d-flex subscription_links">
					<li class="wp-block-button is-style-fill w-50">
						<a class="wp-block-button__link initial" href="<?php echo $button_link_1_1; ?>"><?php _e("Sign up", "maatwerkonline"); ?><i class="ahc ahc-arrow-right pl-2"></i></a>
					</li>
					<a class="link my-auto pl-4 w-50" href="<?php echo $button_link_2_1; ?>"><?php _e("Try it first", "maatwerkonline"); ?></a>
				</div>
			</div>
		</div>

		<div class="col-12 col-md-4 py-3 py-md-0">
			<div class="subscription">
				<img src="<?php echo $usp_image_2; ?>" class="most-chosen"></img>
				<h3 class="capital"><?php echo $title_2; ?></h3>
				<div class="price pb-4">
					<h3 class="pr-2 mb-0"><?php echo $price_2; ?></h3>
					<p class="mt-auto mb-0"><?php echo $price_description_2; ?></p>
				</div>
				<ul class="subscription__list pb-3">
					<?php foreach ($list_items_2 as $list_item) : ?>
						<li class="<?php if ( $list_item['included'] ) { echo 'included'; } else { echo 'excluded'; } ?>"><?php echo $list_item['content']; ?> <?php if ($list_item['tooltip']) { ?> <i class="ahc ahc-info"><div class="list-description"><p class="mb-0"><?php echo $list_item['tooltip_content']; ?></p></div></i> <?php } ?></li>
					<?php endforeach; ?>
				</ul>
				<div class="d-flex subscription_links">
					<li class="wp-block-button is-style-fill w-50">
						<a class="wp-block-button__link initial" href="<?php echo $button_link_1_2; ?>"><?php _e("Sign up", "maatwerkonline"); ?><i class="ahc ahc-arrow-right pl-2"></i></a>
					</li>
					<a class="link my-auto pl-4 w-50" href="<?php echo $button_link_2_2; ?>"><?php _e("Try it first", "maatwerkonline"); ?></a>
				</div>
			</div>
		</div>

		<div class="col-12 col-md-4 py-3 py-md-0">
			<div class="subscription">
				<img src="<?php echo $usp_image_3; ?>" class="best-result">
				<h3 class="capital"><?php echo $title_3; ?></h3>
				<div class="price pb-4">
					<h3 class="pr-2 mb-0"><?php echo $price_3; ?></h3>
					<p class="mt-auto mb-0"><?php echo $price_description_3; ?></p>
				</div>
				<ul class="subscription__list pb-3">
					<?php foreach ($list_items_3 as $list_item) : ?>
						<li class="<?php if ( $list_item['included'] ) { echo 'included'; } else { echo 'excluded'; } ?>"><?php echo $list_item['content']; ?> <?php if ($list_item['tooltip']) { ?> <i class="ahc ahc-info"><div class="list-description"><p class="mb-0"><?php echo $list_item['tooltip_content']; ?></p></div></i> <?php } ?></li>
					<?php endforeach; ?>
				</ul>
				<div class="d-flex subscription_links">
					<li class="wp-block-button is-style-fill w-50">
						<a class="wp-block-button__link blue initial" href="<?php echo $button_link_1_3; ?>"><?php _e("Sign up", "maatwerkonline"); ?><i class="ahc ahc-arrow-right pl-2"></i></a>
					</li>
					<a class="link my-auto pl-4 w-50" href="<?php echo $button_link_2_3; ?>"><?php _e("Try it first", "maatwerkonline"); ?></a>
				</div>
			</div>
		</div>

		<div class="col-12 pt-5 d-flex">
			<h6 class="mb-0 text-white ml-auto"><?php _e("Not the subscriptions for you?", "maatwerkonline"); ?></h6>
			<a class="text-white mr-auto pl-2" href="<?php echo $subscription_link; ?>"><h6><u><?php _e("Look at all our subscriptions!", "maatwerkonline"); ?></u></h6></a>
		</div>

		<img src="<?php echo $background_image; ?>" class="subscription-image alignfull">

	</div>
</div>