<?php
namespace FlyingPress;

class Compatibility
{
  public static $active_non_compatible_plugins = [];

  public static function init()
  {
    add_action('admin_init', ['FlyingPress\Compatibility', 'update_non_compatible_plugins']);

    add_action('admin_notices', ['FlyingPress\Compatibility', 'add_notice']);
  }

  public static function update_non_compatible_plugins()
  {
    $non_compatible_plugins = [
      [
        'name' => 'Autoptimize',
        'file' => 'autoptimize/autoptimize.php',
      ],
      [
        'name' => 'SG Optimizer',
        'file' => 'sg-cachepress/sg-cachepress.php',
      ],
      [
        'name' => 'WP Rocket',
        'file' => 'wp-rocket/wp-rocket.php',
      ],
      [
        'name' => 'LiteSpeed Cache',
        'file' => 'litespeed-cache/litespeed-cache.php',
      ],
      [
        'name' => 'Swift Performance',
        'file' => 'swift-performance/performance.php',
      ],
      [
        'name' => 'Swift Performance Lite',
        'file' => 'swift-performance-lite/performance.php',
      ],
      ['name' => 'Breeze', 'file' => 'breeze/breeze.php'],
      [
        'name' => 'W3 Total Cache',
        'file' => 'w3-total-cache/w3-total-cache.php',
      ],
      [
        'name' => 'WP Fastest Cache',
        'file' => 'wp-fastest-cache/wpFastestCache.php',
      ],
      [
        'name' => 'WP Super Cache',
        'file' => 'wp-super-cache/wp-cache.php',
      ],
      [
        'name' => 'Hummingbird',
        'file' => 'hummingbird-performance/wp-hummingbird.php',
      ],
      [
        'name' => 'Cache Enabler',
        'file' => 'cache-enabler/cache-enabler.php',
      ],
      [
        'name' => 'Fast Velocity Minify',
        'file' => 'fast-velocity-minify/fvm.php',
      ],
    ];

    // Generate a list of non-compatible active plugin
    foreach ($non_compatible_plugins as $plugin) {
      if (\is_plugin_active($plugin['file'])) {
        array_push(self::$active_non_compatible_plugins, $plugin['name']);
      }
    }
  }

  public static function add_notice()
  {
    // Return if there are no plugins
    if (!count(self::$active_non_compatible_plugins)) {
      return;
    }

    echo '<div class="notice notice-error is-dismissible"><p>The following plugins are not compatible with FlyingPress:<br/><ul>';

    foreach (self::$active_non_compatible_plugins as $plugin) {
      echo "<li><b>• $plugin</b></li>";
    }

    echo '</ul></p></div>';
  }
}
