<?php
/**
 * Block Name: Banner Tekst
 * This is the template that displays the block banner-tekst.
 *
 * @author Maatje van Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Banner tekst block -->
	<?php
	$background_image = get_field('background_image');
	$title = get_field('title');
	$content = get_field('content');
	$button_1 = get_field('button_1');
	$button_2 = get_field('button_2');
	?>
	<div class="banner-text-container" style="background-image: url(<?php echo $background_image['url']; ?>);">
	
		<div class="banner-text-container__background-image-overlay"></div>

		<div class="banner-text-container__content-wrapper content-holder">
			<?php if ( function_exists('yoast_breadcrumb') ) {  // If Yoast Breadcrumbs is enabled on /wp-admin/admin.php?page=wpseo_titles#top#breadcrumbs, show the breadcrumbs
				yoast_breadcrumb( '<p class="mb-0 pb-3 breadcrumbs" id="breadcrumbs">','</p>' );
			}; ?>
			<h2 class="text-white mb-0"><?php echo $title; ?></h2>
			<div class="banner-text-container__content-wrapper__content text-white"><?php echo $content; ?></div>
			
			<div class="d-flex mt-4 buttons">
				<li class="wp-block-button is-style-fill">
					<a class="wp-block-button__link" href="<?php echo $button_1['url']; ?>"><?php echo $button_1['title']; ?><i class="ahc ahc-arrow-right pl-2"></i></a>
				</li>
				<li class="wp-block-button is-style-outline-secondary ml-3">
					<a class="wp-block-button__link" href="<?php echo $button_2['url']; ?>"><?php echo $button_2['title']; ?></a>
				</li>
			</div>
		
		</div>

	</div>
</div>