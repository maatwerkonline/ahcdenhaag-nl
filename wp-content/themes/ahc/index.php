<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php 
$archive_pages_settings = get_option( 'archive-page-settings' );
$archive_page_current_post_type = $archive_pages_settings[get_queried_object()->name];

if( !empty( $archive_page_current_post_type ) ){
	$page = get_post( $archive_page_current_post_type );
	echo apply_filters( 'the_content', $page->post_content );
}; ?>

<?php get_footer();
