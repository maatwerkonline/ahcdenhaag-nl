<?php
/**
 * Block Name: Instructor
 * This is the template that displays the block instructor.
 *
 * @author Maatje van Maatwerk Online
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Instructor block -->

	<?php 
	$instructors = get_field('instructor');
	?>

	<div class="wp-bootstrap-blocks-row row">

		<?php
		foreach ($instructors as $instructor) : 
			?>

			<div class="col-12 col-md-4 instructor-container py-3">
				<div class="d-flex">
					<img class="instructor-container__image" src="<?php echo $instructor['image']; ?>">
					<div class="instructor-container__content mt-auto">
						<h3 class="mb-0"><?php echo $instructor['name']; ?></h3>
						<p class="mb-0"><?php echo $instructor['function']; ?></p>
					</div>
				</div>
				<p class="pt-3"><?php echo $instructor['information']; ?></p>
			</div>

			<?php 
		endforeach; 
		?>

	</div>

</div>