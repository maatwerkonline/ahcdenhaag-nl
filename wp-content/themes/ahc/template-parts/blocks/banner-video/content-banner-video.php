<?php
/**
 * Block Name: Banner Video
 * This is the template that displays the block banner-video.
 *
 * @author Maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Banner video block -->
	<?php
	$video = get_field('video');
	$title = get_field('title');
	$description = get_field('description');
	$button_1 = get_field('button_1');
	$button_2 = get_field('button_2');
	?>
	<div class="video-container">
		<video class="video-container__video" src="<?php echo $video; ?>" autoplay muted loop></video>
		<div class="video-container__fade"></div>
		<div class="video-container__content">
			<h1 class="text-white mb-0"><?php echo $title; ?></h1>
			<h3 class="text-white mb-0"><?php echo $description; ?></h3>
			<div class="d-md-flex pt-5 buttons">
				<li class="wp-block-button is-style-fill mb-3 mb-md-0">
					<a class="wp-block-button__link" href="<?php echo $button_1['url']; ?>"><?php echo $button_1['title']; ?><i class="ahc ahc-arrow-right pl-2"></i></a>
				</li>
				<li class="wp-block-button is-style-outline-secondary ml-3">
					<a class="wp-block-button__link" href="<?php echo $button_2['url']; ?>"><?php echo $button_2['title']; ?></a>
				</li>
			</div>
		</div>

		<a href="#" class="video-container__down text-white d-flex"><i class="ahc ahc-arrow-down m-auto pt-1"></i></a>

	</div>
</div>