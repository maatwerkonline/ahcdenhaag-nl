<?php
namespace FlyingPress;

use Alc\SitemapCrawler;

class Ajax
{
  public static function init()
  {
    add_action('wp_ajax_cached_pages', ['FlyingPress\Ajax', 'cached_pages']);
    add_action('wp_ajax_save_config', ['FlyingPress\Ajax', 'save_config']);
    add_action('wp_ajax_activate_license', ['FlyingPress\Ajax', 'activate_license']);
    add_action('wp_ajax_update_license', ['FlyingPress\Ajax', 'update_license']);
    add_action('wp_ajax_purge_cached_pages', ['FlyingPress\Ajax', 'purge_cached_pages']);
    add_action('wp_ajax_purge_current_page', ['FlyingPress\Ajax', 'purge_current_page']);
    add_action('wp_ajax_preload_cache', ['FlyingPress\Ajax', 'preload_cache']);
    add_action('wp_ajax_purge_css_js_fonts', ['FlyingPress\Ajax', 'purge_css_js_fonts']);
    add_action('wp_ajax_purge_entire_cache', ['FlyingPress\Ajax', 'purge_entire_cache']);
    add_action('wp_ajax_database_stats', ['FlyingPress\Ajax', 'database_stats']);
    add_action('wp_ajax_database_clean', ['FlyingPress\Ajax', 'database_clean']);
  }

  public static function cached_pages()
  {
    $cached_pages = Cache::get_cached_pages_count();
    echo $cached_pages;
    wp_die();
  }

  public static function save_config()
  {
    $config = json_decode(stripslashes($_POST['config']), true);
    Config::update_config($config);
    Cache::setup_lifespan();
    Database::setup_scheduled_clean();
    Lifecycle::add_advanced_cache();
    echo 'Done';
    wp_die();
  }

  public static function activate_license()
  {
    $config = json_decode(stripslashes($_POST['config']), true);
    $license_key = $config['license_key'];
    $license_beta = $config['license_beta'];
    $response = License::activate_license($license_key, $license_beta);
    wp_send_json($response);
    wp_die();
  }

  public static function update_license()
  {
    $response = License::update_license();
    wp_send_json($response);
    wp_die();
  }

  public static function purge_cached_pages()
  {
    Purge::purge_cached_pages();
    echo 'Done';
    wp_die();
  }

  public static function purge_current_page()
  {
    $url = $_POST['url'];
    Purge::purge_by_urls([$url]);
    echo 'Done';
    wp_die();
  }

  public static function preload_cache()
  {
    Preload::preload_cache();
    echo 'Done';
    wp_die();
  }

  public static function purge_css_js_fonts()
  {
    Purge::purge_css_js_fonts();
    echo 'Done';
    wp_die();
  }

  public static function purge_entire_cache()
  {
    Purge::purge_entire_cache();
    echo 'Done';
    wp_die();
  }

  public static function database_stats()
  {
    $stats = Database::stats();
    echo json_encode($stats);
    wp_die();
  }

  public static function database_clean()
  {
    Database::clean();
    echo 'Done';
    wp_die();
  }
}
