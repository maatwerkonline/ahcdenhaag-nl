// Load Responsive Images
(function () {
  const dpi = window.devicePixelRatio > 1 ? 2 : 1;
  const images = document.querySelectorAll('img[data-origin-src]');
  images.forEach(function (image) {
    const image_width = image.offsetWidth * dpi;
    const render_width = Math.ceil(image_width / 100) * 100;
    const origin_src = image.getAttribute('data-origin-src');
    image.src = `${origin_src}?width=${render_width}`;
  });
})();

// Load on User Interaction
(function () {
  const events = ['mouseover', 'keydown', 'touchstart', 'touchmove', 'wheel'];
  const elements = document.querySelectorAll("[data-lazy-method='interaction']");

  if (!elements.length) return;

  function load_elements() {
    clearTimeout(timeout);
    events.forEach(function (event) {
      window.removeEventListener(event, load_elements, { passive: true });
    });

    elements.forEach(function (element) {
      element
        .getAttribute('data-lazy-attributes')
        .split(',')
        .forEach(function (attribute) {
          const value = element.getAttribute('data-lazy-'.concat(attribute));
          element.setAttribute(attribute, value);
        });
    });
  }

  const timeout = setTimeout(load_elements, 10000);

  events.forEach(function (event) {
    window.addEventListener(event, load_elements, { passive: true });
  });
})();

// Load Elements in Viewport
(function () {
  const observer = new IntersectionObserver(
    function (elements) {
      elements.forEach(function (element) {
        if (element.isIntersecting) {
          observer.unobserve(element.target);
          element.target
            .getAttribute('data-lazy-attributes')
            .split(',')
            .forEach(function (attribute) {
              const value = element.target.getAttribute('data-lazy-'.concat(attribute));
              element.target.setAttribute(attribute, value);
            });
        }
      });
    },
    { rootMargin: '300px' },
  );
  document.querySelectorAll("[data-lazy-method='viewport']").forEach(function (element) {
    observer.observe(element);
  });
})();
