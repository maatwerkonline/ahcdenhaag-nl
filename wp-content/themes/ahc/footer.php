<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
				</div>
				<footer id="footer">
					<div class="wp-bootstrap-blocks-container container">
						<div class="wp-bootstrap-blocks-row row footer-nav-container-1">
							<div class="col-12 col-md-3 pb-4 pb-md-0 footer-nav-1"><?php dynamic_sidebar( __('First Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-12 col-md-3 pb-4 pb-md-0"><?php dynamic_sidebar( __('Second Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-12 col-md-3 pb-4 pb-md-0"><?php dynamic_sidebar( __('Third Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-12 col-md-3 pb-4 pb-md-0"><?php dynamic_sidebar( __('Fourth Footer Widget', 'onm_textdomain') );?></div>
						</div>
						<div class="wp-bootstrap-blocks-row row pt-md-5 footer-nav-container-2">
							<div class="col-6 col-md-3 pb-4 pb-md-0"><?php dynamic_sidebar( __('Fifth Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-6 col-md-3 pb-4 pb-md-0"><?php dynamic_sidebar( __('Sixth Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-6 col-md-3 pb-4 pb-md-0"><?php dynamic_sidebar( __('Seventh Footer Widget', 'onm_textdomain') );?></div>
							<div class="col-6 col-md-3 pb-4 pb-md-0"><?php dynamic_sidebar( __('Eighth Footer Widget', 'onm_textdomain') );?></div>
						</div>
					</div>
				</footer>
				<div class="container trademark alignfull">
					<div class="row">
						<div class="footer-trademark d-flex">
							<div class="mx-auto d-flex">
								<p class="m-0 text-white"><?php _e('Website by', 'maatwerkonline'); ?></p> 
								<a href="https://www.maatwerkonline.nl/" target="_blank"><img class="trademark-logo" src="<?php echo get_site_url(); ?>/wp-content/themes/ahc/maatwerk-online.png" alt="Maatwerk Online Logo"></a>
						</div>
					</div>
				</div>
			</div> <!-- end #page-content-wrapper -->

		</div> <!-- end #wrapper -->

		<?php wp_footer(); ?>

	</body>
</html>
