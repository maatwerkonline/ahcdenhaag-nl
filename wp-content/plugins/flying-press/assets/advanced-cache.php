<?php

// FlyingPress

if (!headers_sent()) {
  // Set response cache headers
  header('x-flying-press-cache: MISS');
  header('x-flying-press-source: PHP');
}

// Check if request method is GET or HEAD
if (!isset($_SERVER['REQUEST_METHOD']) || !in_array($_SERVER['REQUEST_METHOD'], ['HEAD', 'GET'])) {
  return false;
}

// Check cookies
if (!empty($_COOKIE)) {
  // Cookies to check (should bypass when found)
  $cookies_regex =
    '/(wordpress_[a-f0-9]+|comment_author|wp-postpass|wordpress_no_cache|wordpress_logged_in|woocommerce_cart_hash|woocommerce_items_in_cart|woocommerce_recently_viewed|edd_items_in_cart)/';
  // Combine cookie keys into a string
  $cookies = implode('', array_keys($_COOKIE));
  // If above cookies are found, bypass
  if (preg_match($cookies_regex, $cookies)) {
    return false;
  }
}

// Check GET variables
if (!empty($_GET)) {
  // Query strings to exclude
  $query_strings_regex = 'QUERY_STRING_REGEX_TO_REPLACE';
  // Bypass if any other query string is found
  if (sizeof(preg_grep($query_strings_regex, array_keys($_GET), PREG_GREP_INVERT)) > 0) {
    return false;
  }
}

// File path of the cached file
$host = $_SERVER['HTTP_HOST'];
$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$cache_file_path = WP_CONTENT_DIR . "/cache/flying-press/$host$path" . 'index.html';

// If we don't have a cache copy, we do not need to proceed
if (!is_readable($cache_file_path)) {
  return false;
}

// Set cache HIT response header
header('x-flying-press-cache: HIT');

// Add Last modified response header
$cache_last_modified = filemtime($cache_file_path);
header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $cache_last_modified) . ' GMT');

// Get last modified since from request header
$http_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])
  ? $_SERVER['HTTP_IF_MODIFIED_SINCE']
  : '';

// If file is not modified during this time, send 304
if ($http_modified_since && strtotime($http_modified_since) >= $cache_last_modified) {
  header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified', true, 304);
  exit();
}

readfile($cache_file_path);
exit();
