<?php
namespace FlyingPress;

class Bloat
{
  public static function init()
  {
    add_action('wp_default_scripts', ['FlyingPress\Bloat', 'disable_jquery_migrate']);
    add_action('init', ['FlyingPress\Bloat', 'disable_emojis']);
  }

  public static function disable_emojis()
  {
    if (!Config::$config['img_disable_emoji']) {
      return;
    }
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', ['FlyingPress\Bloat', 'disable_emojis_tinymce']);
  }

  public static function disable_emojis_tinymce($plugins)
  {
    if (is_array($plugins)) {
      return array_diff($plugins, ['wpemoji']);
    } else {
      return [];
    }
  }

  public static function disable_jquery_migrate($scripts)
  {
    if (!Config::$config['js_disable_jquery_migrate']) {
      return;
    }

    if (!is_admin() && !empty($scripts->registered['jquery'])) {
      $scripts->registered['jquery']->deps = array_diff($scripts->registered['jquery']->deps, [
        'jquery-migrate',
      ]);
    }
  }
}