<?php
/**
 * Block Name: Image link
 * This is the template that displays the block image-link.
 *
 * @author Maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?>"> <!-- Image link block -->
	<?php
	$link = get_field('link');
	$image = get_field('image');
	$title = get_field('title');
	?>
	<a href="<?php echo $link; ?>">
		<div class="image-link-container" style="background-image:url('<?php echo $image; ?>');">
			<div class="overlay"></div>
			<div class="content">
				<h3 class="text-white mb-1"><?php echo $title; ?></h3>
				<span><?php _e('Read more', 'maatwerkonline'); ?><i class="ahc ahc-arrow-right pl-2"></i></span>
			</div>
		</div>
	</a>
</div>