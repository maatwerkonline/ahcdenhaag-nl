<?php
namespace FlyingPress;

class Cache
{
  private static $default_exclude_keywords = [
    '/login',
    '/cart',
    '/checkout',
    '/account',
    '/my-account',
    '/wp-admin',
    '/feed',
    '.xml',
    '.txt',
    '.php',
  ];

  private static $default_ignore_queries = [
    'age-verified',
    'ao_noptimize',
    'usqp',
    'cn-reloaded',
    'sscid',
    'ef_id',
    's_kwcid',
    '_bta_tid',
    '_bta_c',
    'dm_i',
    'fb_action_ids',
    'fb_action_types',
    'fb_source',
    'fbclid',
    'utm_id',
    'utm_source',
    'utm_campaign',
    'utm_medium',
    'utm_expid',
    'utm_term',
    'utm_content',
    '_ga',
    'gclid',
    'campaignid',
    'adgroupid',
    'adid',
    '_gl',
    'gclsrc',
    'gdfms',
    'gdftrk',
    'gdffi',
    '_ke',
    'trk_contact',
    'trk_msg',
    'trk_module',
    'trk_sid',
    'mc_cid',
    'mc_eid',
    'mkwid',
    'pcrid',
    'mtm_source',
    'mtm_medium',
    'mtm_campaign',
    'mtm_keyword',
    'mtm_cid',
    'mtm_content',
    'msclkid',
    'epik',
    'pp',
    'pk_source',
    'pk_medium',
    'pk_campaign',
    'pk_keyword',
    'pk_cid',
    'pk_content',
    'redirect_log_mongo_id',
    'redirect_mongo_id',
    'sb_referer_host',
    'ref',
  ];

  public static function init()
  {
    add_filter('cron_schedules', ['FlyingPress\Cache', 'add_cron_hours']);
    add_action('flying_press_preload_cache', ['FlyingPress\Preload', 'preload_cache']);
  }

  public static function add_cron_hours($schedules)
  {
    if (!isset($schedules['2hours'])) {
      $schedules['2hours'] = [
        'interval' => 2 * 60 * 60,
        'display' => 'Once every 2 hours',
      ];
    }
    if (!isset($schedules['6hours'])) {
      $schedules['6hours'] = [
        'interval' => 6 * 60 * 60,
        'display' => 'Once every 6 hours',
      ];
    }
    return $schedules;
  }

  public static function setup_lifespan()
  {
    $lifespan = Config::$config['cache_lifespan'];
    $action = 'flying_press_preload_cache';

    // Clear
    if ($lifespan === 'never') {
      wp_clear_scheduled_hook($action);
    }
    // Not scheduled yet, schedule
    elseif (!wp_next_scheduled($action)) {
      wp_schedule_event(time(), $lifespan, $action);
    }
    // Already scheduled, clear existing and schedule
    else {
      wp_clear_scheduled_hook($action);
      wp_schedule_event(time(), $lifespan, $action);
    }
  }

  public static function is_page_excluded()
  {
    $current_url = home_url($_SERVER['REQUEST_URI']);
    $keywords = Config::$config['cache_excludes'];
    $keywords = array_merge($keywords, self::$default_exclude_keywords);
    if (Utils::any_keywords_match_string($keywords, $current_url)) {
      return true;
    }
    return false;
  }

  public static function is_url_allowed($url)
  {
    $keywords = Config::$config['cache_excludes'];
    $keywords = array_merge($keywords, self::$default_exclude_keywords);
    if (Utils::any_keywords_match_string($keywords, $url)) {
      return false;
    }
    return true;
  }

  public static function is_page_cacheable()
  {
    // Check if caching is disabled
    if (!Config::$config['cache']) {
      return false;
    }

    // Prevent caching if user is logged in
    if (is_user_logged_in()) {
      return false;
    }

    // Prevent caching 404 pages
    if (is_404()) {
      return false;
    }

    // Check if request method is GET
    if (!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD'] != 'GET') {
      return false;
    }

    // Check cookies
    if (!empty($_COOKIE)) {
      // Cookies to check (should bypass when found)
      $cookies_regex =
        '/(wordpress_[a-f0-9]+|comment_author|wp-postpass|wordpress_no_cache|wordpress_logged_in|woocommerce_cart_hash|woocommerce_items_in_cart|woocommerce_recently_viewed|edd_items_in_cart)/';
      // Combine cookie keys into a string
      $cookies = implode('', array_keys($_COOKIE));
      // If above cookies are found, bypass
      if (preg_match($cookies_regex, $cookies)) {
        return false;
      }
    }

    // Check GET variables
    if (!empty($_GET)) {
      // Query strings to exclude
      $query_strings_regex = self::get_ignore_queries_regex();
      // Bypass if any other query string is found
      if (sizeof(preg_grep($query_strings_regex, array_keys($_GET), PREG_GREP_INVERT)) > 0) {
        return false;
      }
    }

    // All looks good, continue to cache
    return true;
  }

  public static function cache_page($html)
  {
    $cache_file_path = FLYING_PRESS_CACHE_DIR . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    @mkdir($cache_file_path, 0755, true);
    // "\xEF\xBB\xBF" is for utf8 encoding
    file_put_contents($cache_file_path . 'index.html', "\xEF\xBB\xBF" . $html);
  }

  public static function get_file_path_from_url($url)
  {
    $file_relative_path = parse_url($url, PHP_URL_PATH);
    $site_path = parse_url(FLYING_PRESS_SITE_URL, PHP_URL_PATH);
    $file_path = FLYING_PRESS_ABSPATH . preg_replace("$^$site_path$", '', $file_relative_path);
    return $file_path;
  }

  public static function get_cached_pages_count()
  {
    $cache_pages = self::rglob(FLYING_PRESS_CACHE_DIR . '*.html', GLOB_BRACE);
    return count($cache_pages);
  }

  public static function get_ignore_queries_regex()
  {
    // E.g.: "utm_source|ref|fbclid"
    $queries = array_merge(Config::$config['cache_ignore_queries'], self::$default_ignore_queries);
    $queries_regex = join('|', $queries);
    $queries_regex = "/^($queries_regex)$/";
    return $queries_regex;
  }

  // Modified version of `glob()` that supports subdirectories
  // Used to count files in a directory
  public static function rglob($pattern, $flags = 0)
  {
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
      $files = array_merge($files, self::rglob($dir . '/' . basename($pattern), $flags));
    }
    return $files;
  }
}