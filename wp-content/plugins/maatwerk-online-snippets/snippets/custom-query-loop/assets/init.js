var initializeSlickSlide = function() {
	window.slickSlide();
}

// Initialize dynamic block preview (editor).
if( window.acf ) {
    window.acf.addAction( 'render_block_preview/type=queryloop', initializeSlickSlide );
}