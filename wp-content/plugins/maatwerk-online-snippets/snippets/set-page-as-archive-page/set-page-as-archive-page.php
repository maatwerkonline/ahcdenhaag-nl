<?php

/**
 * Snippet Name: Set Page as Archive Page
 * Description: Makes it possible to Set a Page as an Archive Page, so you can edit the Archive page through Pages.
 * Version: 1.0
 * Author: Nick Heurter
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Register Setting for Archive Pages
 */
function mo_archive_pages_register_settings() {

    // Update all settings
    register_setting( 'archive-pages-settings', 'archive-page-settings' );
}
add_action( 'admin_init', 'mo_archive_pages_register_settings' );


/**
 * Admin Menu for Archive Pages
 */
function mo_archive_pages_admin_menu() {
    add_theme_page( 
        __('Archief Pagina\'s', 'mos'),
        __('Archief Pagina\'s', 'mos'),
        'manage_options',
        'archive-pages',
        'mo_archive_pages_settings',
    );
}
add_action( 'admin_menu', 'mo_archive_pages_admin_menu' );


/**
 * Settings for Archive Pages
 */
function mo_archive_pages_settings() { 
    ?>

    <div class="wrap">

        <h1><?php _e('Archief Pagina\'s', 'mos'); ?></h1>

        <form action="options.php" method="POST">
   
            <table class="form-table" role="presentation">
                <tbody>
                
                    <?php settings_fields( 'archive-pages-settings' ); ?>
                    <?php do_settings_sections( 'archive-pages-settings' ); ?>
                    <?php $options = get_option( 'archive-page-settings' ); ?>
                    
                    <?php $args = array(
                            'public'   => true,
                        );
                        $post_types = get_post_types( $args, 'objects', 'and' );
                        
                        if ( $post_types ) { // If there are any custom public post types.
                            foreach ( $post_types  as $post_type ) { 
                                if( !empty( get_post_type_archive_link( $post_type->name ) ) ) { ?>
                               
                                    <tr>
                                        <th scope="row">
                                            <label for="archive_page_<?php echo $post_type->name; ?>"><?php _e('Archief pagina voor', 'mos'); ?> <?php echo $post_type->label; ?></label>
                                        </th>

                                        <td>
                                            <select name='archive-page-settings[<?php echo $post_type->name; ?>]' id="archive_page_<?php echo $post_type->name; ?>">
                                                <option value='' <?php if( !empty( $options[ $post_type->name ] ) ) { selected( $options[ $post_type->name ], '' ); }; ?>><?php _e('Standaard archiefpagina', 'mos'); ?></option>
                                                <?php 
                                                $pages = get_pages(); 
                                                foreach ( $pages as $page ) { ?>
                                                    <option value="<?php echo $page->ID; ?>" <?php if( !empty( $options[ $post_type->name ] ) ) { selected( $options[ $post_type->name ], $page->ID ); }; ?>><?php echo $page->post_title; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="description"><?php printf( __('is %1$sde archiefpagina%2$s voor %3$s.', 'mos'), '<a href="' . esc_url( get_post_type_archive_link( $post_type->name ) ) . '" target="_blank">', '</a>', $post_type->label ); ?></span>
                                        </td>
                                    
                                    </tr>

                                <?php }
                            }
                        }
                        ?>

                </tbody>
            </table>

            <?php submit_button(); ?>

        </form>

    </div>

    <?php
}


/**
 * Change slug to Post Type slug for Saved Archive Pages.
 */
function mo_change_slug_for_archive_pages( $old_value, $value, $option ){

    $archive_pages_settings = get_option( 'archive-page-settings' );
                        
    if ( !empty( $archive_pages_settings ) ) {
        foreach ( $archive_pages_settings as $post_type => $page_id ) { 
            if ( $post_type != 'post' ) {
                $post_type_object = get_post_type_object($post_type);
                if ( !empty( $page_id ) ) {
                    wp_update_post(
                        array (
                            'ID'        => $page_id,
                            'post_name' => $post_type_object->rewrite['slug'],
                        )
                    );
                    add_post_meta(  $page_id, 'query', $post_type );
                }
            }
        }
    }
}
add_action('update_option_archive-page-settings', 'mo_change_slug_for_archive_pages', 10, 3);


/**
 * Change Archive Template to page.php file for Saved Archive Pages.
 */
function mo_set_saved_archive_pages($archive_template){
    $archive_pages_settings = get_option( 'archive-page-settings' );
                        
    if ( $archive_pages_settings ) { // If there are any custom public post types.
        foreach ( $archive_pages_settings as $post_type => $page_id ) { 
            if ( !empty( $page_id ) ) {
                $archive_template = get_template_directory() . '/index.php';
                add_post_meta(  $page_id, 'query', $post_type );
            }
        }
    }
    return $archive_template;

}
add_filter( 'archive_template', 'mo_set_saved_archive_pages' ) ;


/**
 * Set Post Status for Saved Archive Pages.
 */
function mo_archive_pages_display_post_states( $post_states, $post ) {
    $archive_pages_settings = get_option( 'archive-page-settings' );
                        
    if ( $archive_pages_settings ) { // If there are any custom public post types.
        foreach ( $archive_pages_settings as $post_type => $page_id ) { 
            if( $post->ID == $page_id ) {
                $post_type_object = get_post_type_object( $post_type );
                $post_states[] = __('Archief pagina van', 'mos') . ' ' . $post_type_object->labels->name;
            }
        }
    }
    return $post_states;
}
add_filter( 'display_post_states', 'mo_archive_pages_display_post_states', 10, 2 );


/**
 * Fire the below functions when Yoast SEO is active.
 * mo_remove_yoast_seo_metabox_for_archive_pages()
 * mo_remove_yoast_seo_sidebar_panel_for_archive_pages()
 */
function mo_yoast_seo_functions_for_pages_as_archive_page() {
    if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) || is_plugin_active( 'wordpress-seo-premium/wp-seo-premium.php' ) ) {
        add_action( 'add_meta_boxes', 'mo_remove_yoast_seo_metabox_for_archive_pages', 11 );
        add_action( 'admin_head-post.php', 'mo_remove_yoast_seo_sidebar_panel_for_archive_pages' );
    }
}
add_action( 'admin_init', 'mo_yoast_seo_functions_for_pages_as_archive_page' );


/**
 * Remove Yoast SEO Metabox for Saved Archive Pages.
 */
function mo_remove_yoast_seo_metabox_for_archive_pages() {
    global $post;

    $archive_pages_settings = get_option( 'archive-page-settings' );
    foreach ( $archive_pages_settings as $post_type => $page_id ) { 
        if( ! empty( $post ) && $page_id == $post->ID ) {
            remove_meta_box( 'wpseo_meta', 'page', 'normal' );
        };
    };
}


/**
 * Remove Yoast SEO Sidebar Panel for Saved Archive Pages.
 */
function mo_remove_yoast_seo_sidebar_panel_for_archive_pages(){
    global $post;
    
    $archive_pages_settings = get_option( 'archive-page-settings' );
    foreach ( $archive_pages_settings as $post_type => $page_id ) { 
        if( ! empty( $post ) && $page_id == $post->ID ) { ?>
            <style>.yoast-seo-sidebar-panel {display: none !important;}</style>
        <?php };
    };
};
