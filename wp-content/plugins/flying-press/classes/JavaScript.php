<?php
namespace FlyingPress;

class JavaScript
{
  public static function init()
  {
    add_action('wp_enqueue_scripts', ['FlyingPress\JavaScript', 'inject_preload_lib']);
  }

  public static function minify($html)
  {
    if (!Config::$config['js_minify']) {
      return true;
    }

    $scripts = $html->find('script[src]');

    foreach ($scripts as $script) {
      // Convert URL to file path
      $file_path = Cache::get_file_path_from_url($script->src);
      if (!is_file($file_path)) {
        continue;
      }

      $hash = substr(hash_file('md5', $file_path), 0, 12);

      // Generate new hashed file name
      $file_name = $hash . '.' . basename($file_path);
      $minified_path = FLYING_PRESS_CACHE_DIR . $file_name;
      $minified_url = FLYING_PRESS_CACHE_URL . $file_name;

      if (!is_file($minified_path)) {
        $minifier = new \MatthiasMullie\Minify\JS($file_path);
        $minifier->minify($minified_path);
      }

      $original_file_size = filesize($file_path);
      $minified_file_size = filesize($minified_path);
      $wasted_bytes = $original_file_size - $minified_file_size;
      $wasted_percent = ($wasted_bytes / $original_file_size) * 100;

      if ($wasted_bytes < 2048 || $wasted_percent < 10 || preg_match('/\.min\.js/', $file_path)) {
        $script->src = strtok($script->src, '?') . "?ver=$hash";
        continue;
      }

      $script->src = $minified_url;
    }
  }

  public static function defer_external($html)
  {
    if (!Config::$config['js_defer']) {
      return true;
    }

    $scripts = $html->find('script[src]');

    $exclude_keywords = Config::$config['js_defer_excludes'];

    foreach ($scripts as $script) {
      if (Utils::any_keywords_match_string($exclude_keywords, $script)) {
        continue;
      }

      $script->defer = '';
      $script->removeAttribute('async');
    }
  }

  public static function defer_inline($html)
  {
    if (!Config::$config['js_defer']) {
      return true;
    }

    if (!Config::$config['js_defer_inline']) {
      return true;
    }

    $scripts = $html->find('script[!src][!type],script[!src][type="text/javascript"]');

    $exclude_keywords = Config::$config['js_defer_excludes'];

    foreach ($scripts as $script) {
      if (!$script->text()) {
        continue;
      }

      if (Utils::any_keywords_match_string($exclude_keywords, $script)) {
        continue;
      }

      $script->src = 'data:text/javascript,' . rawurlencode($script->text());
      $script->type = 'text/javascript';
      $script->defer = '';
      $script->setInnerHtml('');
    }
  }

  public static function delay_scripts($html)
  {
    if (!Config::$config['js_interaction']) {
      return;
    }

    $keywords = Config::$config['js_interaction_includes'];

    $scripts = $html->find(
      'script[src], script[!src][!type], script[!src][type="text/javascript"]'
    );

    foreach ($scripts as $script) {
      if (Utils::any_keywords_match_string($keywords, $script)) {
        $script->{'data-lazy-src'} = $script->src
          ? $script->src
          : 'data:text/javascript,' . rawurlencode($script->text());
        $script->{'data-lazy-method'} = 'interaction';
        $script->{'data-lazy-attributes'} = 'src';
        $script->removeAttribute('src');
        $script->setInnerHtml('');
      }
    }
  }

  public static function inject_core_lib($html)
  {
    $js_code = file_get_contents(FLYING_PRESS_PLUGIN_DIR . 'assets/core.min.js');
    $script_tag = $html->createElement('script', $js_code);
    $html->first('body')->appendChild($script_tag);
  }

  public static function inject_preload_lib()
  {
    if (!Config::$config['js_preload_links']) {
      return;
    }

    // Disable for logged admins
    if (current_user_can('manage_options')) {
      return;
    }

    // Return  if the response is AMP since custom JavaScript isn't allowed
    if (function_exists('is_amp_endpoint') && is_amp_endpoint()) {
      return;
    }

    wp_enqueue_script(
      'flying_press_preload',
      FLYING_PRESS_PLUGIN_URL . 'assets/preload.min.js',
      [],
      FLYING_PRESS_VERSION,
      true
    );
    wp_script_add_data('flying_press_preload', 'defer', true);
  }
}