<?php
namespace FlyingPress;

class Database
{
  public static function init()
  {
    add_action('cron_schedules', ['FlyingPress\Database', 'add_monthly_cron']);
    add_action('flying_press_clean_database', ['FlyingPress\Database', 'clean']);
  }

  public static function add_monthly_cron($schedules)
  {
    $schedules['monthly'] = [
      'display' => 'Once monthly',
      'interval' => 2635200,
    ];
    return $schedules;
  }

  public static function setup_scheduled_clean()
  {
    $schedule = Config::$config['db_schedule_clean'];
    $action = 'flying_press_clean_database';

    // Clear
    if ($schedule === 'never') {
      wp_clear_scheduled_hook($action);
    }
    // Not scheduled yet, schedule
    elseif (!wp_next_scheduled($action)) {
      wp_schedule_event(time(), $schedule, $action);
    }
    // Already scheduled, clear existing and schedule
    else {
      wp_clear_scheduled_hook($action);
      wp_schedule_event(time(), $schedule, $action);
    }
  }

  public static function stats()
  {
    global $wpdb;

    $stats = [];

    $stats['db_post_revisions'] = $wpdb->get_var(
      "SELECT COUNT(ID) FROM $wpdb->posts WHERE post_type = 'revision'"
    );

    $stats['db_post_auto_drafts'] = $wpdb->get_var(
      "SELECT COUNT(ID) FROM $wpdb->posts WHERE post_status = 'auto-draft'"
    );

    $stats['db_post_trashed'] = $wpdb->get_var(
      "SELECT COUNT(ID) FROM $wpdb->posts WHERE post_status = 'trash'"
    );

    $stats['db_comments_spam'] = $wpdb->get_var(
      "SELECT COUNT(comment_ID) FROM $wpdb->comments WHERE comment_approved = 'spam'"
    );

    $stats['db_comments_trashed'] = $wpdb->get_var(
      "SELECT COUNT(comment_ID) FROM $wpdb->comments WHERE (comment_approved = 'trash' OR comment_approved = 'post-trashed')"
    );

    $time = isset($_SERVER['REQUEST_TIME']) ? (int) $_SERVER['REQUEST_TIME'] : time();
    $stats['db_transients_expired'] = $wpdb->get_var(
      $wpdb->prepare(
        "SELECT COUNT(option_name) FROM $wpdb->options WHERE option_name LIKE %s AND option_value < %d",
        $wpdb->esc_like('_transient_timeout') . '%',
        $time
      )
    );

    $stats['db_transients_all'] = $wpdb->get_var(
      $wpdb->prepare(
        "SELECT COUNT(option_id) FROM $wpdb->options WHERE option_name LIKE %s OR option_name LIKE %s",
        $wpdb->esc_like('_transient_') . '%',
        $wpdb->esc_like('_site_transient_') . '%'
      )
    );

    $stats['db_optimize_tables'] = $wpdb->get_var(
      "SELECT COUNT(table_name) FROM information_schema.tables WHERE table_schema = '" .
        DB_NAME .
        "' and Engine <> 'InnoDB' and data_free > 0"
    );

    return $stats;
  }

  public static function clean()
  {
    global $wpdb;

    if (Config::$config['db_post_revisions']) {
      $query = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_type = 'revision'");
      if ($query) {
        foreach ($query as $id) {
          wp_delete_post_revision(intval($id)) instanceof \WP_Post ? 1 : 0;
        }
      }
    }

    if (Config::$config['db_post_auto_drafts']) {
      $query = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_status = 'auto-draft'");
      if ($query) {
        foreach ($query as $id) {
          wp_delete_post(intval($id), true) instanceof \WP_Post ? 1 : 0;
        }
      }
    }

    if (Config::$config['db_post_trashed']) {
      $query = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE post_status = 'trash'");
      if ($query) {
        foreach ($query as $id) {
          wp_delete_post($id, true) instanceof \WP_Post ? 1 : 0;
        }
      }
    }

    if (Config::$config['db_comments_spam']) {
      $query = $wpdb->get_col(
        "SELECT comment_ID FROM $wpdb->comments WHERE comment_approved = 'spam'"
      );
      if ($query) {
        foreach ($query as $id) {
          wp_delete_comment(intval($id), true);
        }
      }
    }

    if (Config::$config['db_comments_trashed']) {
      $query = $wpdb->get_col(
        "SELECT comment_ID FROM $wpdb->comments WHERE (comment_approved = 'trash' OR comment_approved = 'post-trashed')"
      );
      if ($query) {
        foreach ($query as $id) {
          wp_delete_comment(intval($id), true);
        }
      }
    }

    if (Config::$config['db_transients_expired']) {
      $time = isset($_SERVER['REQUEST_TIME']) ? (int) $_SERVER['REQUEST_TIME'] : time();
      $query = $wpdb->get_col(
        $wpdb->prepare(
          "SELECT option_name FROM $wpdb->options WHERE option_name LIKE %s AND option_value < %d",
          $wpdb->esc_like('_transient_timeout') . '%',
          $time
        )
      );
      if ($query) {
        foreach ($query as $transient) {
          $key = str_replace('_transient_timeout_', '', $transient);
          delete_transient($key);
        }
      }
    }

    if (Config::$config['db_transients_all']) {
      $query = $wpdb->get_col(
        $wpdb->prepare(
          "SELECT option_name FROM $wpdb->options WHERE option_name LIKE %s OR option_name LIKE %s",
          $wpdb->esc_like('_transient_') . '%',
          $wpdb->esc_like('_site_transient_') . '%'
        )
      );
      if ($query) {
        foreach ($query as $transient) {
          if (strpos($transient, '_site_transient_') !== false) {
            delete_site_transient(str_replace('_site_transient_', '', $transient));
          } else {
            delete_transient(str_replace('_transient_', '', $transient));
          }
        }
      }
    }

    if (Config::$config['db_optimize_tables']) {
      $query = $wpdb->get_results(
        "SELECT table_name, data_free FROM information_schema.tables WHERE table_schema = '" .
          DB_NAME .
          "' and Engine <> 'InnoDB' and data_free > 0"
      );
      if ($query) {
        foreach ($query as $table) {
          $wpdb->query("OPTIMIZE TABLE $table->table_name");
        }
      }
    }
  }
}