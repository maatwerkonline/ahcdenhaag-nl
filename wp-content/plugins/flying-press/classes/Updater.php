<?php
namespace FlyingPress;

class Updater
{
  public static function init()
  {
    if (!class_exists('EDD_SL_Plugin_Updater')) {
      // load our custom updater.
      include dirname(FLYING_PRESS_FILE_PATH) . '/lib/EDD_SL_Plugin_Updater.php';
    }

    // Check if license key is set
    if (!isset(Config::$config['license_key'])) {
      return;
    }

    $edd_updater = new \EDD_SL_Plugin_Updater(FLYING_PRESS_EDD_STORE_URL, FLYING_PRESS_FILE_PATH, [
      'version' => FLYING_PRESS_VERSION,
      'license' => Config::$config['license_key'],
      'item_id' => FLYING_PRESS_EDD_ITEM_ID,
      'author' => 'Gijo Varghese',
      'beta' => Config::$config['license_beta'],
    ]);
  }
}
