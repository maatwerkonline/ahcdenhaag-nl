<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

	$categories = wp_get_post_categories(get_the_id());
	$author_id = get_the_author_ID(get_the_id());
	$author = get_author_name($author_id);

	?>

	<div class="alignfull pb-5">

		<div class="banner-text-container" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
		
			<div class="banner-text-container__background-image-overlay"></div>

			<div class="banner-text-container__content-wrapper content-holder">
				<?php if ( function_exists('yoast_breadcrumb') ) {  // If Yoast Breadcrumbs is enabled on /wp-admin/admin.php?page=wpseo_titles#top#breadcrumbs, show the breadcrumbs
					yoast_breadcrumb( '<p class="mb-0 pb-3 breadcrumbs" id="breadcrumbs">','</p>' );
				}; ?>
				<h2 class="text-white mb-0"><?php echo get_the_title(); ?></h2>
				<div class="banner-text-container__content-wrapper__content text-white"><?php echo get_the_excerpt(); ?></div>
				<?php if($categories) : ?>
					<div class="blog_article__content_holder__image__categories">
						<?php foreach($categories as $category) :
							$cat = get_category( $category );
							$cat_id = get_cat_ID( $cat->name );
							$cat_ids[] = get_cat_ID( $cat->name );
							?>
							<span href="<?php echo get_site_url(); ?>/boostz/blog/?term=<?php echo $cat_id; ?>" class="mr-3"><?php echo $cat->name; ?></span>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<div class="d-flex pt-2">
					<p class="text-white"><?php echo $author; ?></p>
					<p class="mb-0 text-white pl-3">
						<svg id="time" xmlns="http://www.w3.org/2000/svg" width="20.078" height="20.078" viewBox="0 0 20.078 20.078">
							<g id="Ellipse_14" data-name="Ellipse 14" fill="none" stroke="#fff" stroke-width="2">
								<circle cx="10.039" cy="10.039" r="10.039" stroke="none"/>
								<circle cx="10.039" cy="10.039" r="9.039" fill="none"/>
							</g>
							<path id="Path_23" data-name="Path 23" d="M686.457,1531.446v6.418l3.871,2.343" transform="translate(-676.709 -1527.396)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"/>
						</svg>
						<span><?php echo get_the_date( 'm-d-Y' ) ?> </span>
					</p>
				</div>
			
			</div>

		</div>

	</div>

	<?php the_content();?>

<?php endwhile; endif;?>

<?php get_footer();