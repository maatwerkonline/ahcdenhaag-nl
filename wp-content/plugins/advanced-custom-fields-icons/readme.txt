=== Advanced Custom Fields: Font Custom Icons ===
Contributors: Maatwerk Online
Tags: Advanced Custom Fields, ACF, Custom Icons
Requires at least: 3.5
Tested up to: 5.3
Stable tag: trunk

Adds a new 'Custom Icons' field to the popular Advanced Custom Fields plugin.

== Description ==
* Specify which Custom Icons


= Compatibility =

This ACF field type is compatible with:
* ACF 5.7+
* Custom Icons

== Installation ==

1. Copy the `advanced-custom-fields-icons` folder into your `wp-content/plugins` folder
2. Activate the Custom Icons plugin via the plugins admin page
3. Create a new field via ACF and select the Custom Icons

== Optional Configuration ==

=== Filters ===


== Changelog ==

= 1.0.0 =
* Initial Release.
