<?php
namespace FlyingPress;

class License
{
  public static function init()
  {
    if (!wp_next_scheduled('flying_press_update_license')) {
      wp_schedule_event(time(), 'weekly', 'flying_press_update_license');
    }
    add_action('admin_notices', ['FlyingPress\License', 'add_license_error']);
    add_action('flying_press_update_license', ['FlyingPress\License', 'update_license']);
  }

  public static function keyless_activate()
  {
    // License key already set, skip activation
    if (Config::$config['license_key']) {
      return;
    }

    // License key not found, skip activation
    if (!defined('FLYING_PRESS_LICENSE_KEY')) {
      return;
    }

    self::activate_license(FLYING_PRESS_LICENSE_KEY);
  }

  public static function add_license_error()
  {
    $license_key = Config::$config['license_key'];
    $license_status = Config::$config['license_status'];
    $status = empty($license_key) ? 'no_license_key' : $license_status;
    $settings_page = admin_url('admin.php?page=flying-press#/license');

    switch ($status) {
      case 'no_license_key':
        echo "<div class='notice notice-error'>
                <p><b>FlyingPress</b>: Activate your license key by going to <a href='$settings_page'>settings</a></p>
              </div>";
        break;
      case 'valid':
        break;
      default:
        echo "<div class='notice notice-error'>
                <p><b>FlyingPress</b>: License key is not valid. Status: $status</p>
              </div>";
    }
  }

  public static function activate_license($license_key, $license_beta = false)
  {
    $response = wp_remote_post(FLYING_PRESS_EDD_STORE_URL, [
      'timeout' => 15,
      'sslverify' => false,
      'body' => [
        'edd_action' => 'activate_license',
        'license' => $license_key,
        'item_id' => FLYING_PRESS_EDD_ITEM_ID,
        'url' => is_multisite() ? network_site_url() : FLYING_PRESS_SITE_URL,
      ],
    ]);

    // make sure the response is successful
    if (is_wp_error($response)) {
      return Config::$config;
    }

    $license = json_decode(wp_remote_retrieve_body($response));
    Config::update_config([
      'license_key' => $license_key,
      'license_status' => $license->license,
      'license_name' => $license->customer_name,
      'license_email' => $license->customer_email,
      'license_expiry' => $license->expires,
      'license_beta' => $license_beta,
    ]);

    return Config::$config;
  }

  public static function update_license()
  {
    $license_key = Config::$config['license_key'];

    $response = wp_remote_post(FLYING_PRESS_EDD_STORE_URL, [
      'timeout' => 15,
      'sslverify' => false,
      'body' => [
        'edd_action' => 'check_license',
        'license' => $license_key,
        'item_id' => FLYING_PRESS_EDD_ITEM_ID,
        'url' => is_multisite() ? network_site_url() : FLYING_PRESS_SITE_URL,
      ],
    ]);

    // make sure the response is successful
    if (is_wp_error($response)) {
      return Config::$config;
    }

    $license = json_decode(wp_remote_retrieve_body($response));

    Config::update_config([
      'license_key' => $license_key,
      'license_status' => $license->license,
      'license_name' => $license->customer_name,
      'license_email' => $license->customer_email,
      'license_expiry' => $license->expires,
    ]);
    return Config::$config;
  }
}