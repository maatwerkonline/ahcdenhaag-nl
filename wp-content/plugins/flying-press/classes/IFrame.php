<?php
namespace FlyingPress;

class IFrame
{
  public static function add_youtube_placeholder($html)
  {
    if (!Config::$config['iframe_youtube_placeholder']) {
      return true;
    }

    $self_host = Config::$config['iframe_youtube_placeholder_self_host'];

    $youtube_iframes = $html->find(
      'iframe[src*="youtu.be"],iframe[src*="youtube.com"],iframe[src*="youtube-nocookie.com"]'
    );

    foreach ($youtube_iframes as $iframe) {
      $srcdoc = self::get_youtube_srcdoc($iframe, $self_host);
      $iframe->srcdoc = $srcdoc;
      $iframe->{'data-src'} = $iframe->src;
      $iframe->removeAttribute('src');
    }
  }

  public static function lazy_load($html)
  {
    if (!Config::$config['iframe_lazyload']) {
      return true;
    }

    $iframes = $html->find('iframe[src],iframe[data-src]');

    foreach ($iframes as $iframe) {
      if ($iframe->srcdoc) {
        $iframe->{'data-lazy-srcdoc'} = $iframe->srcdoc;
        $iframe->{'data-lazy-src'} = $iframe->src ?? $iframe->{'data-src'};
        $iframe->{'data-lazy-method'} = 'viewport';
        $iframe->{'data-lazy-attributes'} = 'srcdoc,src';
        $iframe->removeAttribute('data-src');
        $iframe->removeAttribute('srcdoc');
        $iframe->removeAttribute('src');
      } else {
        $iframe->{'data-lazy-src'} = $iframe->src ?? $iframe->{'data-src'};
        $iframe->{'data-lazy-method'} = 'viewport';
        $iframe->{'data-lazy-attributes'} = 'src';
        $iframe->removeAttribute('data-src');
        $iframe->removeAttribute('src');
      }
    }
  }

  public static function get_youtube_srcdoc($video, $self_host)
  {
    $video->src .= preg_match('/\?/', $video->src) ? '&autoplay=1' : '?autoplay=1';
    $video_id = self::get_youtube_video_id($video->src);
    $video_image_url = "https://img.youtube.com/vi/$video_id/hqdefault.jpg";

    if ($self_host) {
      $video_image_url = self::self_host_placeholder($video_id);
    }

    return "<style>body{overflow:hidden}img{margin:auto;width:100%;position:absolute;inset:0;height:auto;overflow:hidden}svg{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);filter:grayscale(100%);opacity:.8}a:hover svg{opacity:1;filter:none}</style><a href=$video->src><img  width=$video->width height=$video->height src='$video_image_url'><svg xmlns='http://www.w3.org/2000/svg' width=68 height=48><path fill=red d='M67 8c-1-3-3-6-6-6-5-2-27-2-27-2S12 0 7 2C4 2 2 5 1 8L0 24l1 16c1 3 3 6 6 6 5 2 27 2 27 2s22 0 27-2c3 0 5-3 6-6l1-16-1-16z'/><path d='M45 24L27 14v20' fill=#fff /></svg></a>";
  }

  public static function self_host_placeholder($video_id)
  {
    $video_image_url = "https://img.youtube.com/vi/$video_id/hqdefault.jpg";
    $cached_image_file = FLYING_PRESS_CACHE_DIR . "$video_id-hqdefault.jpg";
    $cached_image_url = FLYING_PRESS_CACHE_URL . "$video_id-hqdefault.jpg";

    if (!is_file($cached_image_file)) {
      $image_response = wp_remote_get($video_image_url);
      $image = wp_remote_retrieve_body($image_response);

      if (!$image) {
        return $video_image_url;
      }

      file_put_contents($cached_image_file, $image);
    }

    return $cached_image_url;
  }

  public static function get_youtube_video_id($url)
  {
    if (preg_match('/(?:\/|=)(.{11})(?:$|&|\?)/', $url, $matches)) {
      return $matches[1];
    }
    return false;
  }
}
