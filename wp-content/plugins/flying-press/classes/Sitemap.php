<?php
namespace FlyingPress;

class Sitemap
{
  public static function get_sitemap_url()
  {
    // Yoast
    if (defined('WPSEO_VERSION') && get_option('wpseo')['enable_xml_sitemap']) {
      return \WPSEO_Sitemaps_Router::get_base_url('sitemap_index.xml');
    }

    // Rank Math
    elseif (defined('RANK_MATH_VERSION') && \RankMath\Helper::is_module_active('sitemap')) {
      return \RankMath\Sitemap\Router::get_base_url('sitemap_index.xml');
    }

    // All in one SEO
    elseif (
      defined('AIOSEOP_VERSION') &&
      isset(get_option('aioseop_options')['modules']) &&
      get_option('aioseop_options')['modules']['aiosp_feature_manager_options'][
        'aiosp_feature_manager_enable_sitemap'
      ] === 'on'
    ) {
      return trailingslashit(home_url()) .
        apply_filters('aiosp_sitemap_filename', 'sitemap') .
        '.xml';
    }

    // The SEO Framework
    elseif (defined('THE_SEO_FRAMEWORK_VERSION') && the_seo_framework()->can_run_sitemap()) {
      return \The_SEO_Framework\Bridges\Sitemap::get_instance()->get_expected_sitemap_endpoint_url();
    }

    // SEO Press
    elseif (defined('SEOPRESS_VERSION') && !!seopress_xml_sitemap_general_enable_option()) {
      return FLYING_PRESS_HOME_URL . '/sitemaps.xml';
    }

    // Google Sitemap Generator
    elseif (defined('SM_SUPPORTFEED_URL')) {
      return FLYING_PRESS_HOME_URL . '/sitemap.xml';
    }

    // XML Sitemap & Google News
    elseif (defined('XMLSF_VERSION')) {
      return FLYING_PRESS_HOME_URL . '/sitemap.xml';
    }

    // Slim SEO
    elseif (defined('SLIM_SEO_VER')) {
      return FLYING_PRESS_HOME_URL . '/sitemap.xml';
    }

    // Squirrly SEO
    elseif (defined('SQ_VERSION')) {
      return FLYING_PRESS_HOME_URL . '/sitemap.xml';
    }

    // Companion Sitemap Generator
    elseif (function_exists('csg_dashboard')) {
      return FLYING_PRESS_HOME_URL . '/sitemap.xml';
    }

    // default
    return FLYING_PRESS_HOME_URL . '/sitemap.xml';
  }

  public static function fetch_sitemap_urls()
  {
    $sitemap_url = Sitemap::get_sitemap_url();
    return Sitemap::crawl_sitemap_recursive($sitemap_url);
  }

  public static function crawl_sitemap_recursive($sitemap_url)
  {
    $xml = file_get_contents($sitemap_url);
    $xml = simplexml_load_string($xml);

    // Error
    if (!$xml) {
      return [];
    }

    $urls = [];

    // Sitemap with direct URls
    foreach ($xml->url as $value) {
      array_push($urls, (string) $value->loc);
    }

    if (count($urls)) {
      return $urls;
    }

    // Sitemap with child sitemaps
    foreach ($xml->sitemap as $value) {
      $urls = array_merge($urls, self::crawl_sitemap_recursive((string) $value->loc));
    }
    return $urls;
  }
}
