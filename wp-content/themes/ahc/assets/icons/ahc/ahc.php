<?php
global $acf_icons_array;
$acf_icons_array['zk'] = array(
'name' => 'Active Health Center Icons',
'icons' => array(
 'ahc-arrow-down' => '<i class="ahc ahc-arrow-down"></i> ahc-arrow-down',
 'ahc-arrow-right' => '<i class="ahc ahc-arrow-right"></i> ahc-arrow-right',
 'ahc-Youtube' => '<i class="ahc ahc-Youtube"></i> ahc-Youtube',
 'ahc-phone' => '<i class="ahc ahc-phone"></i> ahc-phone',
 'ahc-mail' => '<i class="ahc ahc-mail"></i> ahc-mail',
 'ahc-Linkedin' => '<i class="ahc ahc-Linkedin"></i> ahc-Linkedin',
 'ahc-Insta' => '<i class="ahc ahc-Insta"></i> ahc-Insta',
 'ahc-FB' => '<i class="ahc ahc-FB"></i> ahc-FB',
 'ahc-pin' => '<i class="ahc ahc-pin"></i> ahc-pin',
 'ahc-check' => '<i class="ahc ahc-check"></i> ahc-check'
)
);