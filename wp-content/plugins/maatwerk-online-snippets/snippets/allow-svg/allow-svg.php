<?php
/**
 * Snippet Name: Allow SVG
 * Description: This Snippet makes it possible to upload SVG files in Wordpress
 * Version: 1.0
 * Author: Frank van 't Hof
 * Required: false
 * Default Active: false
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
	exit;
}

function allow_svg_upload_mimes( $mimes ) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'allow_svg_upload_mimes' );
