<?php
namespace FlyingPress;

class Preload
{
  public static function preload_urls($urls)
  {
    $urls = array_filter($urls, ['FlyingPress\Cache', 'is_url_allowed']);
    @mkdir(FLYING_PRESS_CACHE_DIR, 0755, true);
    file_put_contents(FLYING_PRESS_CACHE_DIR . 'preload.json', json_encode(array_values($urls)));
    Preload::preload_url(Preload::get_next_url());
  }

  public static function preload_cache()
  {
    Purge::purge_cached_pages();
    $urls = Sitemap::fetch_sitemap_urls();
    $urls = array_filter($urls, ['FlyingPress\Cache', 'is_url_allowed']);
    @mkdir(FLYING_PRESS_CACHE_DIR, 0755, true);
    file_put_contents(FLYING_PRESS_CACHE_DIR . 'preload.json', json_encode(array_values($urls)));
    Preload::preload_url(Preload::get_next_url());
  }

  public static function remove_page_from_preload()
  {
    $url = home_url($_SERVER['REQUEST_URI']);
    $preload_file = FLYING_PRESS_CACHE_DIR . 'preload.json';
    $urls = @json_decode(file_get_contents($preload_file)) ?? [];
    $urls = array_diff($urls, [$url]);
    file_put_contents(FLYING_PRESS_CACHE_DIR . 'preload.json', json_encode(array_values($urls)));
  }

  public static function get_next_url()
  {
    $preload_file = FLYING_PRESS_CACHE_DIR . 'preload.json';
    $urls = @json_decode(file_get_contents($preload_file)) ?? [];
    $url_to_preload = array_shift($urls);
    file_put_contents($preload_file, json_encode(array_values($urls)));
    return $url_to_preload;
  }

  public static function preload_url($url)
  {
    if (!isset($url)) {
      return;
    }

    wp_remote_get($url, [
      'timeout' => 0.01,
      'blocking' => false,
      'sslverify' => false,
      'headers' => ['x-flying-press-preload' => true],
      'user-agent' => 'FlyingPress',
    ]);
  }
}
