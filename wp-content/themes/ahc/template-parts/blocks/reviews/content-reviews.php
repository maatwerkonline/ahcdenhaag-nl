<?php
/**
 * Block Name: Reviews
 * This is the template that displays the block reviews.
 *
 * @author Maatwerkonline
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 * @param   object $block The block jQuery element.
 * @param   object attributes The block attributes (only available when editing).
 *
 */

// Convert name ("acf/{block_name}") into path friendly slug ("{block_name}")
$slug = str_replace('acf/', '', $block['name']);

// Create id attribute allowing for custom "anchor" value.
$id = $slug . '-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$classes = $slug;

if( !empty($block['className']) ) {
	$classes .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$classes .= ' align' . $block['align'];
}
if( !empty($block['align_text']) ) {
	$classes .= ' align-text-' . $block['align_text'];
}
if( !empty($block['align_content']) ) {
	$classes .= ' is-position-' . $block['align_content'];
}
?>
 
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($classes); ?> review-slider"> <!-- Review block -->
	<?php
	$reviews = get_field('reviews');
	foreach ($reviews as $review) : ?>
		<div class="wp-bootstrap-blocks-row row review d-flex">
			<div class="col-12 col-md-6 review__images">
				<img src="<?php echo $review['image']; ?>">
				<div class="review-green-circle"></div>
			</div>
			<div class="col-12 col-md-6 review__content d-flex">
				<div class="m-auto">
					<h3 class="text-uppercase pb-3"><?php echo $review['name']; ?></h3>
					<p class="pb-3"><?php echo $review['review']; ?></p>
					<li class="wp-block-button is-style-fill">
						<a class="wp-block-button__link" href="<?php echo $review['link']; ?>"><?php _e("Try it yourself!", "maatwerkonline"); ?> <i class="ahc ahc-arrow-right pl-2"></i></a>
					</li>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>