<?php

/*
 * Snippet Name: Page Dependencies
 * Description: Adds page dependencies settings
 * Version: 1.0
 * Author: Nick Heurter
 * Required: true
 * Default Active: true
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


function admin_init_page_dependencies() {
    // Add a 'Required' button to all not yet Required pages
    function dependence_pages_button() {

        if(is_maatwerkonline()){ // If the current user is info@maatwerkonline.nl
            add_filter( 'page_row_actions', 'page_dependency_link', 10, 2 );
        };

    };
    add_action( 'admin_head-edit.php', 'dependence_pages_button' );


    // Dependency Link
    function page_dependency_link( $actions, $post ) {
        $required_pages = unserialize( get_option( 'page_dependencies_list' ) );

            if ( $required_pages && in_array( $post->ID, $required_pages ) ) { 
                $actions['theme_dependency'] = '<a class="disable-required" href="#!">' . __( 'Disable Required', 'maatwerkonline' ) . '</a>';
            } else {
                $actions['theme_dependency'] = '<a class="enable-required" href="#!">' . __( 'Enable Required', 'maatwerkonline' ) . '</a>';
            };

        return $actions;
    };

    // Check if WooCommerce plugin is active
    if (is_plugin_active('woocommerce/woocommerce.php')) {

        // If the Shop Page is changed, remove the dependency of the old Shop page and add the dependency for the new Shop page
        function update_page_dependencies_list_shop_page( $old_value, $value ){
            $pages = unserialize( get_option( 'page_dependencies_list' ) );

            if( $value ){
                $pages[] = $value;
            }
            if( $old_value ){
                $remove_page_id = $old_value;
                if ( ( $key = array_search($remove_page_id, $pages ) ) !== false ) {
                    unset($pages[$key]);
                };
            };
            
            update_option( 'page_dependencies_list', serialize($pages) );
        };
        add_action('update_option_woocommerce_shop_page_id', 'update_page_dependencies_list_shop_page', 10, 2);


        // If the Cart Page is changed, remove the dependency of the old Cart page and add the dependency for the new Cart page
        function update_page_dependencies_list_cart_page( $old_value, $value ){
            $pages = unserialize( get_option( 'page_dependencies_list' ) );

            if( $value ){
                $pages[] = $value;
            }
            if( $old_value ){
                $remove_page_id = $old_value;
                if ( ( $key = array_search($remove_page_id, $pages ) ) !== false ) {
                    unset($pages[$key]);
                };
            };
            
            update_option( 'page_dependencies_list', serialize($pages) );
        };
        add_action('update_option_woocommerce_cart_page_id', 'update_page_dependencies_list_cart_page', 10, 2);


        // If the Checkout Page is changed, remove the dependency of the old Checkout page and add the dependency for the new Checkout page
        function update_page_dependencies_list_checkout_page( $old_value, $value ){
            $pages = unserialize( get_option( 'page_dependencies_list' ) );

            if( $value ){
                $pages[] = $value;
            }
            if( $old_value ){
                $remove_page_id = $old_value;
                if ( ( $key = array_search($remove_page_id, $pages ) ) !== false ) {
                    unset($pages[$key]);
                };
            };
            
            update_option( 'page_dependencies_list', serialize($pages) );
        };
        add_action('update_option_woocommerce_checkout_page_id', 'update_page_dependencies_list_checkout_page', 10, 2);


        // If the My Account Page is changed, remove the dependency of the old My Account page and add the dependency for the new My Account page
        function update_page_dependencies_list_myaccount_page( $old_value, $value ){
            $pages = unserialize( get_option( 'page_dependencies_list' ) );

            if( $value ){
                $pages[] = $value;
            }
            if( $old_value ){
                $remove_page_id = $old_value;
                if ( ( $key = array_search($remove_page_id, $pages ) ) !== false ) {
                    unset($pages[$key]);
                };
            };
            
            update_option( 'page_dependencies_list', serialize($pages) );
        };
        add_action('update_option_woocommerce_myaccount_page_id', 'update_page_dependencies_list_myaccount_page', 10, 2);


        // If the Terms Page is changed, remove the dependency of the old Terms page and add the dependency for the new Terms page
        function update_page_dependencies_list_terms_page( $old_value, $value ){
            $pages = unserialize( get_option( 'page_dependencies_list' ) );

            if( $value ){
                $pages[] = $value;
            }
            if( $old_value ){
                $remove_page_id = $old_value;
                if ( ( $key = array_search($remove_page_id, $pages ) ) !== false ) {
                    unset($pages[$key]);
                };
            };
            
            update_option( 'page_dependencies_list', serialize($pages) );
        };
        add_action('update_option_woocommerce_terms_page_id', 'update_page_dependencies_list_terms_page', 10, 2);
    }


    // Ajax Update Dependencies List Function
    function update_page_dependencies_list(){
        $pages = unserialize( get_option( 'page_dependencies_list' ) );

        if( $_POST['add_page_id'] ){
            $pages[] = $_POST['add_page_id'];
        } else {
            $remove_page_id = $_POST['remove_page_id'];
            if ( ( $key = array_search($remove_page_id, $pages ) ) !== false ) {
                unset($pages[$key]);
            };
        };
        
        update_option( 'page_dependencies_list', serialize($pages) );
        exit();
    }
    add_action( 'wp_ajax_update_page_dependencies_list', 'update_page_dependencies_list' );


    // jQuery script for saving the data on click trough the update_page_dependencies_list() function
    function script_update_page_dependencies_list(){
        global $pagenow;

        if (get_post_type() == 'page') {  // Only execute on Page Post Types to speed-up the admin on other CPT

            if( is_maatwerkonline() ){  // If the current user is info@maatwerkonline.nl ?>

                <style type="text/css">.enable-required:before,.disable-required:before {content: '';height: 10px;width: 10px;border-radius: 50%;background-color: #86d641;display: inline-block;margin-right: 7px;}.disable-required:before{background-color: #e2473e;}</style>
                <script type="text/javascript">
                    jQuery(".enable-required, .disable-required").on("click", function(){
                        var $this = jQuery(this);
                        var $required_page = $this.closest("tr").attr("id").replace(/[^0-9]/gi, "");

                        $this
                            .css( "pointer-events", "none" ) // Make sure the button can not be clicked twice
                            .before( "<img class='loader' style='width: 16px;height: 16px; padding: 0 10px 0 0; position: relative; top: 3px;' src='/wp-admin/images/spinner.gif'>" ); // Add the Native WordPress Loader as an indication for the user

                        if($this.hasClass("enable-required")){ // If the 'Enable Required' button is clicked
                            var add_page_id = $required_page;
                        } else { // If the 'Disable Required' button is clicked
                            var remove_page_id = $required_page;
                        }

                        jQuery.ajax({
                            type: "POST",
                            data:{
                                action: "update_page_dependencies_list",
                                add_page_id: add_page_id,
                                remove_page_id: remove_page_id
                            },
                            url: ajaxurl,
                            success: function(result){
                                $this.prev().remove(); // Remove the Loading icon
                                if($this.hasClass("enable-required")){ // If the 'Enable Required' button was clicked
                                    $this
                                        .css("pointer-events", "") // Make sure the button can be clicked again
                                        .text("<?php _e( 'Disable Required', 'maatwerkonline' ); ?>") // Change the text to 'Disable Required'
                                        .addClass("disable-required") // Change the class to 'disable-required'
                                        .removeClass("enable-required"); // Change the class 'inable-required'
                                    var $data_page_selector = '#post-'+$required_page+' a.submitdelete';
                                    var $data_page_selector_hover_before = '#post-'+$required_page+' a.submitdelete:hover:before';
                                    var $data_page_selector_hover_after = '#post-'+$required_page+' a.submitdelete:hover:after';
                                    jQuery('head').append('<style type="text/css" id="style-dependence-post-' + $required_page + '">' + $data_page_selector + '{position: relative;color: #82878c;cursor: default;}' + $data_page_selector + ' .locked-indicator-icon:before{display: inline;}' + $data_page_selector_hover_before + '{content:"<?php _e( 'You\'re not allowed to trash this required page.', 'maatwerkonline'); ?>";position: absolute;bottom: -25px;left: -10px;width: max-content;background: #23292d;color: #eee;font-size: 10px;padding: 0 5px;border-radius: 3px;}' + $data_page_selector_hover_after + '{content: "";position: absolute;bottom: -6px;left: 0;width: 0;height: 0;border-left: 5px solid transparent;border-right: 5px solid transparent;border-bottom: 5px solid #23292d;}</' + 'style>');

                                } else { // If the 'Disable Required' button was clicked
                                    $this
                                        .css("pointer-events", "") // Make sure the button can be clicked again
                                        .text("<?php _e( 'Enable Required', 'maatwerkonline' ); ?>") // Change the text to 'Enable Required' 
                                        .addClass("enable-required") // Change the class to 'enable-required'
                                        .removeClass("disable-required"); // Change the class 'disable-required'
                                    jQuery('#style-dependence-post-' + $required_page + '').remove();
                                };
                                console.log(result);
                            },
                            error: function(error){
                                $this.prev().remove(); // Remove the Loading icon
                                console.log(error);
                            }
                        });
                    });
                </script>

            <?php };
            
        };
    };
    add_action( 'admin_print_footer_scripts-edit.php', 'script_update_page_dependencies_list' );


    // jQuery scripts for hidding the 'Trash' link for Required Pages for not info@maatwerkonline.nl users and add a confirmation when clicking the 'Trash' link for Required Pages
    function hide_trash_page_link() {
        global $pagenow;
        $required_pages = unserialize( get_option( 'page_dependencies_list' ) );

        if (get_post_type() == 'page') { // Only execute on Page Post Types to speed-up the admin on other CPT ?>

            <script type="text/javascript">jQuery('a.submitdelete').prepend('<span class="locked-indicator-icon" aria-hidden="true"></span>');</script><style type="text/css">a.submitdelete .locked-indicator-icon:before {display: none; color: #82878c;font-size: 12px;position: relative;top: -1px;margin-right: 5px;}</style> <?php // Add a locked icon to all Trash buttons ?>

            <?php
            // Walk Trough all Required Pages
            if ( $required_pages ) {
                foreach ( $required_pages as $required_page ) {
                    $data_page_selector = '#post-'.$required_page.' a.submitdelete';
                    $data_page_selector_hover_before = '#post-'.$required_page.' a.submitdelete:hover:before';
                    $data_page_selector_hover_after = '#post-'.$required_page.' a.submitdelete:hover:after'; ?>

                    <style type="text/css" id="style-dependence-post-<?php echo $required_page; ?>"><?php echo $data_page_selector; ?>{position: relative;color: #82878c;cursor: default;}<?php echo $data_page_selector; ?> .locked-indicator-icon:before{display: inline;}<?php echo $data_page_selector_hover_before; ?>{content:"<?php _e('You\'re not allowed to trash this required page.', 'maatwerkonline'); ?>";position: absolute;bottom: -25px;left: -10px;width: max-content;background: #23292d;color: #eee;font-size: 10px;padding: 0 5px;border-radius: 3px;}<?php echo $data_page_selector_hover_after; ?>{content: "";position: absolute;bottom: -6px;left: 0;width: 0;height: 0;border-left: 5px solid transparent;border-right: 5px solid transparent;border-bottom: 5px solid #23292d;}</style>
                    
                    <?php 

                };
            };

        };
    };
    add_action( 'admin_print_footer_scripts-edit.php', 'hide_trash_page_link' );


    // jQuery scripts for hidding the 'Trash' link for Required Pages for not info@maatwerkonline.nl users and add a confirmation when clicking the 'Trash' link for Required Pages
    function hide_trash_edit_page_link() {
        global $pagenow;
        $required_pages = unserialize( get_option( 'page_dependencies_list' ) );

        $post_ID = get_the_ID();

        if (get_post_type() == 'page') {  // Only execute on Page Post Types to speed-up the admin on other CPT

            // Walk Trough all Required Pages
            if ( $required_pages ) {
                if ( in_array( $post_ID, $required_pages ) ) { ?>
                    <style type="text/css">.editor-post-trash{pointer-events: none; color: #82878b !important;}.editor-post-trash:before {color: #82878c;content: "\f160";display: inline-block;font: normal 20px/1 dashicons;speak: none;vertical-align: middle;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;margin-right: 10px;position: relative;top: -1px;}</style>
                <?php };
            };

        };
    };
    add_action('admin_print_footer_scripts-post.php', 'hide_trash_edit_page_link');
    add_action('admin_print_footer_scripts-post-new.php', 'hide_trash_edit_page_link');


    // Block Remove for Required Pages
    function restrict_required_page_deletion($post_ID){
        $required_pages = unserialize( get_option( 'page_dependencies_list' ) );

        if ( in_array( $post_ID, $required_pages ) ) { 
            wp_die( sprintf( __('Your theme depends on the page %1$s. So you are not allowed the Trash this page.', 'maatwerkonline'), get_the_title($post_ID) ) );
        };
    }
    add_action( 'wp_trash_post', 'restrict_required_page_deletion', 10, 1 );


    // Block Status change to any other status then Publish for Required Pages
    function deperestrict_required_page_status_changendencie ($post_ID, $data ) {
        $required_pages = unserialize( get_option( 'page_dependencies_list' ) );

        if ( 'nav-menus' !== get_post_type($post_ID) ) { // Check if current Post Type is not Nav Menus
            return;
        }

        $post_status = isset( $data['post_status'] ) ? $data['post_status'] : get_post_status( $post_ID );

        if ( in_array( $post_ID, $required_pages ) && ($post_status != 'publish') ) { 
            wp_die( sprintf( __('Your theme depends on the page %1$s. So you are not allowed to unpublish this page.', 'maatwerkonline'), get_the_title($post_ID) ) );
        };
    };
    add_action( 'pre_post_update', 'deperestrict_required_page_status_changendencie', 10, 2);
    }
add_action( 'admin_init', 'admin_init_page_dependencies' );